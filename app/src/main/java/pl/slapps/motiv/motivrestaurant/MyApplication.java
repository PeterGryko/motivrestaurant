package pl.slapps.motiv.motivrestaurant;

import android.app.Application;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.motiv.motivrestaurant.model.Driver;
import pl.slapps.motiv.motivrestaurant.model.Order;
import pl.slapps.motiv.motivrestaurant.model.Place;

/**
 * Created by piotr on 17.03.15.
 */
public class MyApplication extends Application {


    public static String token;


    public static Place currentRestaurant;
    public static JSONArray currentCrew;




    public static String getTokenUserId(){
        JSONObject data = null;
        String id = "";
        try {
            data = new JSONObject(SmartPitAppHelper.deserializeJsonWebToken(token));
            id = data.has("_id")?data.getString("_id"):"";
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return id;
    }

    public static String getTokenUserRole(){
        JSONObject data = null;
        String role = "";
        try {
            data = new JSONObject(SmartPitAppHelper.deserializeJsonWebToken(token));
            role = data.has("role")?data.getString("role"):"";
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return role;
    }





}
