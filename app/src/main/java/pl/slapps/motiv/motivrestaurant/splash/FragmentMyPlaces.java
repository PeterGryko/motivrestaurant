package pl.slapps.motiv.motivrestaurant.splash;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.fragment.FragmentMainMenu;
import pl.slapps.motiv.motivrestaurant.model.Place;
import pl.slapps.motiv.motivrestaurant.widget.AppHelper;

/**
 * Created by piotr on 26.06.15.
 */
public class FragmentMyPlaces extends SmartPitFragment {

    private SmartPitSelectorView btnAdd;
    private ListView lv;
    private String TAG = FragmentMyPlaces.class.getName();
    private AdapterMyPlaces adapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_my_places, parent, false);
        lv = (ListView) v.findViewById(R.id.lv);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                lv.setEnabled(false);
/*
                DAO.getRestaurantData(new Response.Listener() {
                    @Override
                    public void onResponse(Object o) {

                        JSONObject data = null;
                        try {
                            data = new JSONObject(o.toString());

                            MyApplication.currentRestaurant = data;
                            ((MainActivity) FragmentMyPlaces.this.getActivity()).switchTitleFragment(new FragmentMainMenu(), true);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }, adapter.getPointId(i));

*/

                JSONObject data = adapter.getPoint(i);
                String id = null;
                try {
                    MyApplication.currentCrew = data.has("_crew") ? data.getJSONArray("_crew") : new JSONArray();

                    data = data.has("place") ? data.getJSONObject("place") : data;

                    id = data.has("_id") ? data.getString("_id") : "";


                    DAO.getRestaurantData(new Response.Listener() {
                        @Override
                        public void onResponse(Object o) {

                            Log.d(TAG, o.toString());
                            try {


                                //  String id= MyApplication.currentRestaurant.id;
                                MyApplication.currentRestaurant = Place.valueOf(new JSONObject(o.toString()));
                                //  MyApplication.currentRestaurant.id=id;


                                if (((MainActivity) FragmentMyPlaces.this.getActivity()).getService() != null)
                                    ((MainActivity) FragmentMyPlaces.this.getActivity()).getService().initBackgroundService();
                                ((MainActivity) FragmentMyPlaces.this.getActivity()).switchTitleFragment(new FragmentMainMenu(), true);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            lv.setEnabled(true);
                        }
                    }, id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        btnAdd = (SmartPitSelectorView) v.findViewById(R.id.btn_add);

        btnAdd.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.plus);
        int padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);
        btnAdd.getImageView().setPadding(padding, padding, padding, padding);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentMyPlaces.this.getFragmentsListener().switchFragment(new FragmentAddPlace(), true);
            }
        });

        DAO.getMyPlaces(new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());


                JSONArray a = null;
                try {
                    JSONObject response = new JSONObject(o.toString());
                    JSONObject api = response.has("api") ? response.getJSONObject("api") : new JSONObject();
                    a = api.has("results") ? api.getJSONArray("results") : new JSONArray();
                    // a = new JSONArray(o.toString());


                    ArrayList<JSONObject> entries = new ArrayList<JSONObject>();

                    for (int i = 0; i < a.length(); i++) {
                        entries.add(a.getJSONObject(i));
                    }
                    adapter = new AdapterMyPlaces(FragmentMyPlaces.this.getActivity(), entries);
                    lv.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, volleyError.toString());
                AppHelper.getErrorMessage(volleyError);
            }
        });
        return v;

    }

    @Override
    public String getLabel() {
        return null;
    }
}
