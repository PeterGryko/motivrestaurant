package pl.slapps.motiv.motivrestaurant.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.android.volley.Response;
import com.android.volley.VolleyError;


import org.json.JSONException;
import org.json.JSONObject;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 15.04.15.
 */
public class FragmentProfile extends SmartPitFragment {

    private SmartImageView logo;
    private EditText et_username;
    private EditText et_email;
    private EditText et_phone;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, parent, false);

        logo = (SmartImageView) v.findViewById(R.id.iv_logo);
        et_username = (EditText) v.findViewById(R.id.et_username);
        et_email = (EditText) v.findViewById(R.id.et_email);
        et_phone = (EditText) v.findViewById(R.id.et_phone);


        DAO.getProfile(new Response.Listener() {
            @Override
            public void onResponse(Object o) {

                try {
                    JSONObject profile = new JSONObject(o.toString());
                    JSONObject data = profile.has("data") ? profile.getJSONObject("data") : new JSONObject();
                    JSONObject avatar = profile.has("avatar") ? profile.getJSONObject("avatar") : new JSONObject();

                    String username = data.has("username") ? data.getString("username") : "";
                    String email = data.has("email") ? data.getString("email") : "";
                    String phone = data.has("phone") ? data.getString("phone") : "";

                    String path = avatar.has("path") ? DAO.API_STATIC + avatar.getString("path") : "";

                    SmartPitImageLoader.setImage(FragmentProfile.this.getActivity(), logo, path, 0, 0);

                    et_phone.setText(phone);
                    et_email.setText(email);
                    et_username.setText(username);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        return v;
    }

    @Override
    public String getLabel() {
        return null;
    }
}
