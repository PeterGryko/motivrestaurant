package pl.slapps.motiv.motivrestaurant.orders;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.fragment.SmartPitPagerFragment;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.background.BackgroundService;
import pl.slapps.motiv.motivrestaurant.model.Order;
import pl.slapps.motiv.motivrestaurant.widget.AppHelper;

/**
 * Created by piotr on 04.10.15.
 */
public class FragmentOrdersLists extends SmartPitPagerFragment {

    private ArrayList<View> fragmentsList;


    private FragmentOrdersList fragmentNew;
    private FragmentOrdersList fragmentToDeliver;
    private FragmentOrdersList fragmentDone;
    private BackgroundService backgroundService;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_orders_lists, parent, false);

        backgroundService = ((MainActivity) this.getActivity()).getService();

        fragmentsList = new ArrayList<View>();

        fragmentNew = new FragmentOrdersList();
        fragmentToDeliver = new FragmentOrdersList();
        fragmentDone = new FragmentOrdersList();

        if (backgroundService != null) {
            ArrayList<Order> orders = backgroundService.getOrders();
            if (!MyApplication.getTokenUserRole().equals("driver")) {
                fragmentsList.add(fragmentNew.init((FragmentOrdersBase) FragmentOrdersLists.this.getParentFragment(), FragmentOrdersLists.this.getFragmentsListener(), AppHelper.filterOrders("new", orders)));
                fragmentsList.add(fragmentDone.init((FragmentOrdersBase) FragmentOrdersLists.this.getParentFragment(), FragmentOrdersLists.this.getFragmentsListener(), AppHelper.filterOrders("production", orders)));
                fragmentsList.add(fragmentToDeliver.init((FragmentOrdersBase) FragmentOrdersLists.this.getParentFragment(), FragmentOrdersLists.this.getFragmentsListener(), AppHelper.filterOrders("to_deliver", orders)));
            } else {
                fragmentsList.add(fragmentNew.init((FragmentOrdersBase) FragmentOrdersLists.this.getParentFragment(), FragmentOrdersLists.this.getFragmentsListener(), AppHelper.filterOrders("to_deliver", orders)));
                fragmentsList.add(fragmentDone.init((FragmentOrdersBase) FragmentOrdersLists.this.getParentFragment(), FragmentOrdersLists.this.getFragmentsListener(), AppHelper.filterOrders("on_way", orders)));
                fragmentsList.add(fragmentToDeliver.init((FragmentOrdersBase) FragmentOrdersLists.this.getParentFragment(), FragmentOrdersLists.this.getFragmentsListener(), AppHelper.filterOrders("done", orders)));

            }

            this.setViewsPager(v, R.id.pager, fragmentsList);
            this.getPager().setOffscreenPageLimit(3);
            this.initMovingIndicator(v, R.id.smart_indicator, inflater.inflate(R.layout.layout_indicator, null), false);
        }
        return v;
    }

    @Override
    public String getLabel() {
        return null;
    }

    public void onPageSelected(int page) {
        super.onPageSelected(page);
        ((FragmentOrdersBase) this.getParentFragment()).refreashMap();
    }

    public View createTabIndicator(Context context, int index) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_tab, null);
        TextView v = (TextView) view.findViewById(R.id.tv_label);
        switch (index) {
            case 0:
                if (!MyApplication.getTokenUserRole().equals("driver"))
                    v.setText(context.getString(R.string.status_new));
                else
                    v.setText(context.getString(R.string.status_to_deliver));
                break;
            case 1:
                if (!MyApplication.getTokenUserRole().equals("driver"))

                    v.setText(context.getString(R.string.status_production));
                else
                    v.setText(context.getString(R.string.on_the_way));
                break;
            case 2:
                if (!MyApplication.getTokenUserRole().equals("driver"))
                    v.setText(context.getString(R.string.status_to_deliver));
                else
                    v.setText(context.getString(R.string.status_done));
                break;
        }


        return view;
    }


    public void refreashData() {
        if (backgroundService != null) {

            ArrayList<Order> orders = backgroundService.getOrders();
            if (!MyApplication.getTokenUserRole().equals("driver")) {
                fragmentNew.refreashData(AppHelper.filterOrders("new", orders));
                fragmentDone.refreashData(AppHelper.filterOrders("production", orders));
                fragmentToDeliver.refreashData(AppHelper.filterOrders("to_deliver", orders));
            } else {
                fragmentNew.refreashData(AppHelper.filterOrders("to_deliver", orders));
                fragmentDone.refreashData(AppHelper.filterOrders("on_way", orders));
                fragmentToDeliver.refreashData(AppHelper.filterOrders("done", orders));
            }
        }
    }
}
