package pl.slapps.motiv.motivrestaurant.background;

/**
 * Created by piotr on 01.05.15.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import io.socket.client.IO;
import io.socket.emitter.Emitter;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;

import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.model.Order;
import pl.slapps.motiv.motivrestaurant.widget.AppHelper;


/**
 * Foreground service. It creates a head view.
 * The pending intent allows to go back to the settings activity.
 */
public class BackgroundService extends Service {

    public interface OnOrdersUpdateListener {
        public void onStatusChanged(JSONObject result);

        public void onCoordsChanged(JSONObject result);

    }

    public interface OnMessageListener {
        public void onNewOrder(JSONObject result);

        public void onNewMessage(JSONObject result);

        public void onUserJoin(JSONObject result);



    }

    private final IBinder mBinder = new LocalBinder();
    private io.socket.client.Socket socket;
    private Thread socketThread;


    private OnOrdersUpdateListener onOrdersUpdateListener;
    private OnMessageListener onMessageListener;

    private ArrayList<Order> orders = new ArrayList<>();
    private String TAG = BackgroundService.class.getName();

    public class LocalBinder extends Binder {
        public BackgroundService getService() {
            return BackgroundService.this;
        }
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOnOrdersUpdateListener(OnOrdersUpdateListener onOrdersUpdateListener) {
        this.onOrdersUpdateListener = onOrdersUpdateListener;
    }

    public void setOnMessage(OnMessageListener onMessage) {
        this.onMessageListener = onMessage;
    }


    @Override
    public IBinder onBind(Intent intent) {

        return mBinder;
    }

    public void initBackgroundService() {
        Log.d(TAG, "on task");
        if (MyApplication.currentRestaurant == null)
            return;

        String id = MyApplication.currentRestaurant.id;
        DAO.getAllOrders(new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());

                parseOrdersResponse(o.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, volleyError.toString());
                Log.d(TAG, "error message " + AppHelper.getErrorMessage(volleyError));
                Toast.makeText(BackgroundService.this, "error loading orders ", Toast.LENGTH_LONG).show();
                // parseOrdersResponse(Order.getMockOrders().toString());
            }
        }, id);
        initSocket();
    }

    public void initSocket() {
        if (socketThread == null) {
            socketThread = new Thread(new SocketThread());
            socketThread.start();
        }

    }

    private void parseOrdersResponse(String o) {

        try {
            JSONObject r = new JSONObject(o.toString());
            JSONObject api = r.has("api") ? r.getJSONObject("api") : new JSONObject();
            JSONArray array = api.has("results") ? api.getJSONArray("results") : new JSONArray();

            orders = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                orders.add(new Order(array.getJSONObject(i)));
            }

            Collections.sort(orders, new Comparator<Order>() {
                @Override
                public int compare(Order lhs, Order rhs) {
                    return (int) AppHelper.gps2m((float) lhs.latitude, (float) lhs.longitude, 0, 0) - (int) AppHelper.gps2m((float) rhs.latitude, (float) rhs.longitude, 0, 0);
                }
            });

            pl.gryko.smartpitlib.widget.Log.d(TAG, "parse orders response " + orders.size());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onDestroy() {

        if (socketThread != null) {
            socketThread.interrupt();
            socketThread = null;
        }
        if (socket != null) {
            socket.disconnect();
            socket.close();
        }
        stopForeground(true);
    }

    private void showNewOrderNotification(Order order) {
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        int icon = R.drawable.black_orders;
        CharSequence notiText = "Nowe zamówienie: " + order.address;
        long meow = System.currentTimeMillis();

        Notification notification = new Notification(icon, notiText, meow);

        Context context = getApplicationContext();
        CharSequence contentTitle = "Nowe zamówienie od: " + order.title;
        CharSequence contentText = order.address;
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);

        int SERVER_DATA_RECEIVED = 1;
        notificationManager.notify(SERVER_DATA_RECEIVED, notification);
    }


    public void emitChangeStatus(String orderId, String status) {
        if (socket == null || !socket.connected()) {
            return;
        }

        try {
            JSONObject s = new JSONObject();
            s.put("to", orderId);
            s.put("status", status);
            socket.emit("statusChange", s);

        } catch
                (Throwable t) {
            Log.d(TAG, t.toString());
        }
    }

    public void emitChangeCoords(double lat, double lng) {
        if (socket == null || !socket.connected()) {
            return;
        }

        try {
            JSONObject coords = new JSONObject();
            coords.put("lat", lat);
            coords.put("lng", lng);
            JSONObject t = new JSONObject();
            t.put("to", MyApplication.currentRestaurant.id);
            t.put("location", coords);
            t.put("driver", MyApplication.getTokenUserId());
            socket.emit("coordsUpdate", t);


        } catch
                (Throwable t) {
            Log.d(TAG, t.toString());
        }
    }

    public void emitMessage(String to, String message) {
        if (socket == null || !socket.connected()) {
            return;
        }

        try {
            JSONObject msg = new JSONObject();
            msg.put("from", MyApplication.currentRestaurant.id);
            msg.put("to", to);
            msg.put("body", message);

            JSONObject senderDetails = new JSONObject();
            msg.put("senderDetails", senderDetails);
            Log.d(TAG, "message emit " + msg);
            socket.emit("msg", msg);


        } catch
                (Throwable t) {
            Log.d(TAG, t.toString());
        }
    }


    class SocketThread implements Runnable {

        public void run() {
            try {


                socket = IO.socket("http://188.166.101.11:7990");
                socket.on(io.socket.client.Socket.EVENT_CONNECT, new Emitter.Listener() {

                    @Override
                    public void call(Object... args) {
                        JSONObject o = new JSONObject();

                        try {


                            o.put("room", MyApplication.currentRestaurant.id);
                            o.put("token", MyApplication.token);
                            o.put("roomType", "restaurant");

                            Log.d(TAG, "emit auth " + o.toString());

                            socket.emit("auth", o);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }).on("join", new Emitter.Listener() {

                    @Override
                    public void call(Object... args) {
                        JSONObject obj = (JSONObject) args[0];
                        if(onMessageListener!=null)
                            onMessageListener.onUserJoin(obj);

                        pl.gryko.smartpitlib.widget.Log.d(TAG, "join " + obj.toString());
                    }

                }).on("error", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {

                        pl.gryko.smartpitlib.widget.Log.d(TAG, "error " + args[0]);
                    }

                })
                        .on("msg", new Emitter.Listener() {
                            @Override
                            public void call(Object... args) {
                                JSONObject obj = (JSONObject) args[0];

                                pl.gryko.smartpitlib.widget.Log.d(TAG, "msg " + args[0]);
                                if (onMessageListener != null)
                                    onMessageListener.onNewMessage(obj);
                            }

                        }).on("statusChange", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        JSONObject obj = (JSONObject) args[0];
                        pl.gryko.smartpitlib.widget.Log.d(TAG, "statusChange " + obj.toString());

                        Order newOrder = new Order(obj);
                        Order oldOrder = AppHelper.getOrder(newOrder.id, orders);
                        if (oldOrder != null)
                            oldOrder.status = newOrder.status;


                        if (onOrdersUpdateListener != null)
                            onOrdersUpdateListener.onStatusChanged(obj);
                    }
                }).on("coordsUpdate", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        JSONObject obj = (JSONObject) args[0];
                        pl.gryko.smartpitlib.widget.Log.d(TAG, "coordsUpdate " + obj.toString());

                        if (onOrdersUpdateListener != null)
                            onOrdersUpdateListener.onStatusChanged(obj);
                    }
                }).on("newOrder", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        JSONObject obj = (JSONObject) args[0];
                        pl.gryko.smartpitlib.widget.Log.d(TAG, "newOrder " + obj.toString());

                        Order order = new Order(obj);
                        orders.add(order);
                        showNewOrderNotification(order);
                        if (onOrdersUpdateListener != null)
                            onOrdersUpdateListener.onStatusChanged(obj);

                        if (onMessageListener != null)
                            onMessageListener.onNewOrder(obj);
                    }
                }).on(io.socket.client.Socket.EVENT_DISCONNECT, new Emitter.Listener() {

                    @Override
                    public void call(Object... args) {

                        pl.gryko.smartpitlib.widget.Log.d(TAG, "event disconenct");
                    }

                }).on(io.socket.client.Socket.EVENT_ERROR, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.d(TAG, "error " + args[0].toString());
                    }
                }).on(io.socket.client.Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.d(TAG, "error connection " + args[0].toString());
                    }
                }).on(io.socket.client.Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.d(TAG, "error timeout " + args[0].toString());
                    }
                });

                IO.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        //TODO: Make this more restrictive
                        Log.d(TAG, "verify " + hostname);
                        return true;
                    }
                });
                socket.connect();
                pl.gryko.smartpitlib.widget.Log.d(TAG, "socket connecting...");
                pl.gryko.smartpitlib.widget.Log.d(TAG, Boolean.toString(socket.connected()));


            } catch (Exception e) {
                android.util.Log.e(TAG, e.toString());
            }
        }

    }


}