package pl.slapps.motiv.motivrestaurant.orders;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 31.08.15.
 */
public class FragmentNewOrderBase extends SmartPitFragment {

    private String TAG = FragmentNewOrderBase.class.getName();


    private FragmentNewOrderGrid fragmentNewOrderGrid;
    private FragmentNewOrderList fragmentNewOrderList;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_new_order_base, parent, false);

        fragmentNewOrderGrid = new FragmentNewOrderGrid();
        fragmentNewOrderGrid.setHost(this);
        fragmentNewOrderList = new FragmentNewOrderList();

        this.getChildFragmentManager().beginTransaction().add(R.id.content_one, fragmentNewOrderList).add(R.id.content_two, fragmentNewOrderGrid).commitAllowingStateLoss();

        return v;
    }

    public void updateListData(JSONObject data) {
        fragmentNewOrderList.updateListData(data);
    }

    @Override
    public String getLabel() {
        return null;
    }
}
