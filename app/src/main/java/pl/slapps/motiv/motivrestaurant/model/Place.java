package pl.slapps.motiv.motivrestaurant.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by piotr on 22.09.15.
 */
public class Place {


    public String id;
    public String place_id;
    public String name;
    public String formatted_address;
    public String formatted_phone_number;
    public String website;

    public String facebookAbout;
    public String facebookBirthday;
    public String facebookCategory;

    public String facebookWebsite;
    public String facebookDesc;
    public int facebookLikes;
    public int facebookTalkingAbout;
    public int facebookWereHere;



    public double lat;
    public double lon;
    public ArrayList<Period> periods;
    public String[] tags;
    public ArrayList<String> photos;


    public static Place valueOf(JSONObject data) {

        Place p = new Place();


        try {


            data = data.has("api") ? data.getJSONObject("api") : data;
            JSONObject google = data.has("google") ? data.getJSONObject("google") : new JSONObject();
            JSONObject facebook = data.has("facebook") ? data.getJSONObject("facebook") : new JSONObject();

            String about = facebook.has("about")?facebook.getString("about"):"";
            String category = facebook.has("category")?facebook.getString("category"):"";
            String birthday = facebook.has("birthday")?facebook.getString("birthday"):"";

            String desc = facebook.has("description")?facebook.getString("description"):"";
            String link = facebook.has("link")?facebook.getString("link"):"";
            int likes = facebook.has("likes")?facebook.getInt("likes"):0;
            int wereHereCount = facebook.has("were_here_count")?facebook.getInt("were_here_count"):0;
            int talkingAbout = facebook.has("talking_about_count")?facebook.getInt("talking_about_count"):0;


            p.facebookAbout=about;
            p.facebookBirthday=birthday;
            p.facebookCategory=category;
            p.facebookDesc=desc;
            p.facebookLikes = likes;
            p.facebookTalkingAbout=talkingAbout;
            p.facebookWebsite=link;
            p.facebookWereHere=wereHereCount;





            JSONArray images = google.has("photos") ? google.getJSONArray("photos") : new JSONArray();
            ArrayList<String> photos = new ArrayList<>();

            for (int i = 0; i < images.length(); i++) {
                photos.add(images.getString(i));
            }

            data = data.has("doc") ? data.getJSONObject("doc") : data;

            String place_id = data.has("_id") ? data.getString("_id") : "";
            String id = data.has("owner") ? data.getString("owner") : "";

            JSONObject openingHours = data.has("opening_hours") ? data.getJSONObject("opening_hours") : new JSONObject();
            JSONArray periods = openingHours.has("periods") ? openingHours.getJSONArray("periods") : new JSONArray();

            ArrayList<Period> parsedPeriods = new ArrayList<>();

            for (int i = 0; i < periods.length(); i++) {
                parsedPeriods.add(new Period(periods.getJSONObject(i)));
            }


            JSONObject geometry = data.has("geometry") ? data.getJSONObject("geometry") : new JSONObject();
            JSONObject location = geometry.has("location") ? geometry.getJSONObject("location") : new JSONObject();
            String name = data.has("name") ? data.getString("name") : "";
            double lat = location.has("lat") ? location.getDouble("lat") : 0;
            double lon = location.has("lng") ? location.getDouble("lng") : 0;

            String formatted_address = data.has("formatted_address") ? data.getString("formatted_address") : "";
            String formatted_phone_number = data.has("formatted_phone_number") ? data.getString("formatted_phone_number") : "";
            String website = data.has("website") ? data.getString("website") : "";

            JSONArray tagsArray = data.has("tags") ? data.getJSONArray("tags") : new JSONArray();
            String tags[] = new String[tagsArray.length()];

            for (int i = 0; i < tagsArray.length(); i++) {
                tags[i] = tagsArray.getString(i);
            }



            p.tags = tags;
            p.formatted_address = formatted_address;
            p.formatted_phone_number = formatted_phone_number;
            p.website = website;
            p.place_id = place_id;
            p.name = name;
            p.lat = lat;
            p.lon = lon;
            p.id = id;
            p.photos = photos;
            p.periods = parsedPeriods;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return p;

    }

}
