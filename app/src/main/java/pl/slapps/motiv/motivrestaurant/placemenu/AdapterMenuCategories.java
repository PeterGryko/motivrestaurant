package pl.slapps.motiv.motivrestaurant.placemenu;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 13.09.15.
 */
public class AdapterMenuCategories extends ArrayAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<JSONObject> data;

    private String active;

    public AdapterMenuCategories(Context context, ArrayList<JSONObject> data) {
        super(context, R.layout.row_menu_category, data);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getItemID(int position) {
        JSONObject element = data.get(position);
        String id = null;
        try {
            id = element.has("_id") ? element.getString("_id") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return id;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        View v = inflater.inflate(R.layout.row_menu_category, null);


        TextView label = (TextView) v.findViewById(R.id.tv_label);
        JSONObject element = data.get(position);

        try {
            String name = element.has("label") ? element.getString("label") : "";

            String id = element.has("_id") ? element.getString("_id") : "";
            label.setText("#" + position + " " + name);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return v;
    }
}
