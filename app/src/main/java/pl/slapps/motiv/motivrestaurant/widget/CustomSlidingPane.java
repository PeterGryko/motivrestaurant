package pl.slapps.motiv.motivrestaurant.widget;

import android.content.Context;
import android.support.v4.widget.SlidingPaneLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by piotr on 18.03.15.
 */
public class CustomSlidingPane extends SlidingPaneLayout {
    public CustomSlidingPane(Context context) {
        super(context);
    }

    public CustomSlidingPane(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSlidingPane(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!
                this.isOpen()) {

            return false;
        } else {
            return super.onInterceptTouchEvent(ev);
        }
    }
}
