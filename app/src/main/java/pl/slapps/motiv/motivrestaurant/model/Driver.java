package pl.slapps.motiv.motivrestaurant.model;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by piotr on 17.03.15.
 */
public class Driver {

    public String username;
    public double latitude;
    public double longitude;
    public String surname;
    public String phone;
    public String email;

    public Driver(JSONObject data) {

        try {
            username = data.has("username") ? data.getString("username") : "";
            surname = data.has("surname") ? data.getString("surname") : "";
            latitude = data.has("latitude") ? data.getDouble("latitude") : 0;
            longitude = data.has("longitude") ? data.getDouble("longitude") : 0;
            email = data.has("email") ? data.getString("email") : "";
            phone = data.has("phone") ? data.getString("phone") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public String getJSONString() {
        String data = new Gson().toJson(this);
        return data;
    }

}
