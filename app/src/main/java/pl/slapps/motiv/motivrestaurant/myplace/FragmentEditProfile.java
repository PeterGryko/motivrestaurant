package pl.slapps.motiv.motivrestaurant.myplace;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.adapter.SmartPitGoogleAddressesAdapter;
import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.model.SmartPitGoogleAddress;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.widget.AppHelper;

/**
 * Created by piotr on 14.04.15.
 */
public class FragmentEditProfile extends SmartPitFragment {

    private String TAG = FragmentEdit.class.getName();

    private ListView listView;
    private ArrayList<JSONObject> dataList;


    private SmartImageView logo;
    private EditText name;
    // private EditText desc;

    private AutoCompleteTextView etAddress;
    private EditText etLat;
    private EditText etLon;

    //private EditText etStreetNr;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.page_edit_profile, parent, false);

        logo = (SmartImageView) v.findViewById(R.id.logo);
        logo.setMode(SmartImageView.Mode.NORMAL.ordinal());
        name = (EditText) v.findViewById(R.id.et_name);
        // desc = (EditText) v.findViewById(R.id.et_desc);

        etAddress = (AutoCompleteTextView) v.findViewById(R.id.et_address);
        etLat = (EditText) v.findViewById(R.id.et_lat);
        etLon = (EditText) v.findViewById(R.id.et_lon);

        // etStreetNr = (EditText) v.findViewById(R.id.et_street_nr);

        //     listView = (ListView) v.findViewById(R.id.lv);


        // dataList = new ArrayList<JSONObject>();
        if (this.getArguments() != null) {
            JSONObject data = null;
            try {
                data = new JSONObject(this.getArguments().getString("data"));
                parseJSONResponse(data);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        return v;
    }

    private void parseJSONResponse(JSONObject data) {

        //JSONObject logo = data.has("logo") ? data.getJSONObject("logo") : new JSONObject();

        //logo.put("logo_element", true);
        //dataList.add(logo);


        try {
            String n = data.has("formatted_address") ? data.getString("formatted_address") : "";
            String snr = data.has("street_nr") ? data.getString("street_nr") : "";
            JSONObject geometry = data.has("geometry") ? data.getJSONObject("geometry") : new JSONObject();
            JSONObject location = geometry.has("location") ? geometry.getJSONObject("location") : new JSONObject();

            double lat = location.has("lat") ? location.getDouble("lat") : 0;
            double lon = location.has("lon") ? location.getDouble("lon") : 0;
            etAddress.setText(n);
            //etStreetNr.setText(snr);
            etLat.setText(Double.toString(lat));
            etLon.setText(Double.toString(lon));

        } catch (JSONException e) {
            e.printStackTrace();
        }


        final SmartPitGoogleAddressesAdapter adapter = new SmartPitGoogleAddressesAdapter(this.getActivity(), android.R.layout.simple_list_item_1);
        etAddress.setAdapter(adapter);
        etAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SmartPitGoogleAddress address = adapter.getPoint(i);
                etLat.setText(Double.toString(address.getLat()));
                etLon.setText(Double.toString(address.getLon()));
                // etStreetNr.setText(address.getStreetNumber());

            }
        });


        String n = null;
        String d = null;
        try {

            JSONObject place = data.has("place") ? data.getJSONObject("place") : data;
            JSONObject place_data = place.has("data") ? place.getJSONObject("data") : place;

            n = place_data.has("name") ? place_data.getString("name") : "";
            d = place_data.has("desc") ? place_data.getString("desc") : "";

            name.setText(n);
            //  desc.setText(d);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        String path = null;
        try {
            path = data.has("path") ? data.getString("path") : "";

            SmartPitImageLoader.setImage(this.getActivity(), logo, DAO.API_NEW + path, 0, 0);

            logo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AppHelper.showPickImageDialog((MainActivity) FragmentEditProfile.this.getActivity(), true);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    @Override
    public String getLabel() {
        return null;
    }
}