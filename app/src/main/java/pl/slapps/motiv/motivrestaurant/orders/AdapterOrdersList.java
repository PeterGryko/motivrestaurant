package pl.slapps.motiv.motivrestaurant.orders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;

import pl.gryko.smartpitlib.interfaces.SmartPitFragmentsInterface;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.model.Order;

/**
 * Created by piotr on 05.05.15.
 */
public class AdapterOrdersList extends ArrayAdapter {

    private String TAG = AdapterOrdersList.class.getName();
    private Context context;
    private ArrayList<Order> list;
    private SmartPitFragmentsInterface listener;

    public AdapterOrdersList(Context context, ArrayList<Order> list, SmartPitFragmentsInterface listener) {
        super(context, R.layout.row_orders_list, list);
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.row_orders_list, null);
        TextView tvAddress = (TextView) convertView.findViewById(R.id.tv_address);
        TextView tvClient = (TextView) convertView.findViewById(R.id.tv_client);
        TextView tvPhone = (TextView) convertView.findViewById(R.id.tv_phone);
        TextView tvPrice = (TextView) convertView.findViewById(R.id.tv_price);

        LinearLayout cbBase = (LinearLayout) convertView.findViewById(R.id.cb_base);
        LinearLayout baseLayout = (LinearLayout) convertView.findViewById(R.id.base_layout);

        /*
        baseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "on item click");
                Bundle arg = new Bundle();
                arg.putString("data", list.get(position).getJSONString());
                FragmentOrderDetail fd = new FragmentOrderDetail();
                fd.setArguments(arg);
                listener.switchFragment(fd, true);
            }
        });
*/
        final CheckBox cb = (CheckBox) convertView.findViewById(R.id.cb);

        cbBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cb.isChecked())
                    cb.setChecked(false);
                else
                    cb.setChecked(true);
            }
        });

        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked())
                    list.get(position).checked = true;
                else
                    list.get(position).checked = false;
            }
        });

        String address;
        if (!list.get(position).on_place)
            address = list.get(position).address;
        else
            address = "Na miejscu";


        tvAddress.setText(address);
        tvClient.setText(list.get(position).title);
        tvPhone.setText(list.get(position).phone);
        tvPrice.setText(list.get(position).price);


        return convertView;

    }
}
