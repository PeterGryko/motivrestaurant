/*
 * Copyright 2012 Terlici Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.slapps.motiv.motivrestaurant.draganddrop;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SimpleAdapter;

import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.motiv.motivrestaurant.model.Order;

public class DragNDropSimpleAdapter extends ArrayAdapter implements DragNDropAdapter {
    //private int mPosition[];
    private int mHandler;
    private List<Order> dataList;
    private String TAG = DragNDropSimpleAdapter.class.getName();

    public DragNDropSimpleAdapter(Context context, List<Order> data, int resource, int handler) {
        super(context, resource, data);

        mHandler = handler;
        dataList = data;
        //setup(data.size());
    }
/*
    private void setup(int size) {
		mPosition = new int[size];

		for (int i = 0; i < size; ++i)
			mPosition[i] = i;
	}
*/


    @Override
    public void onItemDrag(DragNDropListView parent, View view, int position, long id) {

    }

    @Override
    public void onItemDrop(DragNDropListView parent, View view, int startPosition, int endPosition, long id) {

        if (startPosition == endPosition)
            return;


        Log.d(TAG, "start position and end position " + startPosition + "  " + endPosition);

        Order o = dataList.get(startPosition);
        dataList.remove(o);
        dataList.add(endPosition, o);

        notifyDataSetChanged();


    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public void notifyDataSetChanged() {
        //setup(dataList.size());
        super.notifyDataSetChanged();
    }

    @Override
    public int getDragHandler() {
        return mHandler;
    }
}
