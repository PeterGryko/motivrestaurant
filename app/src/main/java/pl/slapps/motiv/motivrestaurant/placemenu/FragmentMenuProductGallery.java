package pl.slapps.motiv.motivrestaurant.placemenu;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.fragment.FragmentGallery;
import pl.slapps.motiv.motivrestaurant.adapter.AdapterGridGallery;

/**
 * Created by piotr on 14.09.15.
 */
public class FragmentMenuProductGallery extends SmartPitFragment {

    private String TAG = FragmentMenuProductGallery.class.getName();
    private GridView gridView;
    private SmartPitSelectorView cameraPick;
    private SmartPitSelectorView galleryPick;
    private JSONObject product;
    private ArrayList<String> gridData;
    private JSONArray galleryData;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menu_product_gallery, parent, false);
        gridView = (GridView) v.findViewById(R.id.grid_view);

        int padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);


        cameraPick = (SmartPitSelectorView) v.findViewById(R.id.btn_camera_file);
        galleryPick = (SmartPitSelectorView) v.findViewById(R.id.btn_gallery_file);

        cameraPick.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.camera_white);
        cameraPick.getImageView().setPadding(padding, padding, padding, padding);

        galleryPick.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.white_gallery);
        galleryPick.getImageView().setPadding(padding, padding, padding, padding);


        ((MainActivity) this.getActivity()).setPickListener(new MainActivity.OnImagePickListener() {
            @Override
            public void onPick(File file) {
                DAO.updateFile(file, new Response.Listener() {
                    @Override
                    public void onResponse(Object o) {
                        Log.d(TAG, o.toString());

                        JSONObject d = null;
                        try {
                            d = new JSONObject(o.toString());

                            JSONObject api = d.has("api") ? d.getJSONObject("api") : new JSONObject();
                            JSONObject doc = api.has("doc") ? api.getJSONObject("doc") : new JSONObject();
                            String id = doc.has("_id") ? doc.getString("_id") : "";

                            saveData(id);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, e.toString());
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d(TAG, volleyError.toString());

                    }
                });
            }
        });

        cameraPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) FragmentMenuProductGallery.this.getActivity()).pickImageFromCamera();
            }
        });
        galleryPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) FragmentMenuProductGallery.this.getActivity()).pickImageFromGallery();
            }
        });


        if (this.getArguments() != null) {
            final String data = this.getArguments().getString("data");

            try {
                product = new JSONObject(data);
                notifyData();


            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

        return v;
    }


    private void saveData(String photoId) {

        Log.d(TAG, "Save data");


        try {
            final String id = product.has("_id") ? product.getString("_id") : "";


            JSONArray photos = product.has("photos") ? product.getJSONArray("photos") : new JSONArray();
            photos.put(photoId);
            product.put("photos", photos);

            DAO.postProduct(new Response.Listener() {
                @Override
                public void onResponse(Object o) {
                    Log.d(TAG, o.toString());
                    Toast.makeText(FragmentMenuProductGallery.this.getActivity(), "Zdjęcie zostało dodane", Toast.LENGTH_LONG).show();
                    refreashData(id);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d(TAG, volleyError.toString());

                }
            }, product, id);

        } catch (Throwable t) {
            Log.d(TAG, t.toString());
        }

    }

    private void notifyData() {

        gridData = new ArrayList<>();
        galleryData = new JSONArray();
        try {
            final JSONArray photos = product.has("photos") ? product.getJSONArray("photos") : new JSONArray();


            for (int i = 0; i < photos.length(); i++) {
                JSONObject element = photos.getJSONObject(i);
                String path = DAO.API_STATIC + (element.has("path") ? element.getString("path") : "");


                gridData.add(path);
                galleryData.put(path);

            }


            gridView.setAdapter(new AdapterGridGallery(this.getActivity(), gridData));
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    ((FragmentGallery) ((MainActivity) FragmentMenuProductGallery.this.getActivity()).getCurrentDrawerFragment()).refreashPager(galleryData.toString(), i);
                    ((MainActivity) FragmentMenuProductGallery.this.getActivity()).showMenu();

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void refreashData(String id) {
        DAO.getProduct(new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                JSONObject data = null;
                try {
                    data = new JSONObject(o.toString());

                    JSONObject api = data.has("api") ? data.getJSONObject("api") : new JSONObject();
                    product = api.has("doc") ? api.getJSONObject("doc") : product;
                    notifyData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }, id);
    }

    @Override
    public String getLabel() {
        return null;
    }
}
