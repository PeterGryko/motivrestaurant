package pl.slapps.motiv.motivrestaurant.widget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.FloatMath;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitAppDialog;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.background.BackgroundService;
import pl.slapps.motiv.motivrestaurant.myplace.FragmentEdit;
import pl.slapps.motiv.motivrestaurant.model.Order;

/**
 * Created by piotr on 17.03.15.
 */
public class AppHelper {

    private static String TAG = AppHelper.class.getName();

    public static void showPickImageDialog(final MainActivity context, final boolean flag) {
        AlertDialog dialog = SmartPitAppDialog.getAlertDialog(AlertDialog.THEME_HOLO_LIGHT, context);
        dialog.setMessage("wybeirz zdjecie");

        dialog.setButton2("galeria", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (flag)
                    FragmentEdit.PICK_LOGO = true;
                else
                    FragmentEdit.PICK_GALLERY = true;


                context.pickImageFromGallery();
                dialog.dismiss();
            }
        });
        dialog.setButton("aparat", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (flag)
                    FragmentEdit.PICK_LOGO = true;
                else
                    FragmentEdit.PICK_GALLERY = true;
                context.pickImageFromCamera();
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    public static void showExitDialog(final MainActivity context) {
        AlertDialog dialog = SmartPitAppDialog.getAlertDialog(AlertDialog.THEME_HOLO_LIGHT, context);
        dialog.setMessage(context.getString(R.string.dialog_exit));

        dialog.setButton2("Tak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                context.finish();
            }
        });
        dialog.setButton("Nie", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        dialog.show();

    }

    public static ArrayList<Order> filterOrders(String state, ArrayList<Order> list) {
        ArrayList<Order> returnList = new ArrayList<Order>();


        for (int i = 0; i < list.size(); i++) {
            Log.d(TAG, list.get(i).status);
            if (state.toLowerCase().trim().equals(list.get(i).status.toLowerCase().trim())) {
                Log.d(TAG, "filtering orders equal " + state + " " + list.get(i).status);
                returnList.add(list.get(i));
            }
        }
        return returnList;
    }

    public static Order getOrder(String arg, ArrayList<Order> orders) {

        try {

            for (int i = 0; i < orders.size(); i++) {
                if (arg.equals(orders.get(i).id)) {
                    return orders.get(i);
                }
            }
        } catch (Throwable t) {
            return null;
        }

        return null;
    }

    public static String getErrorMessage(VolleyError volleyError) {
        String out = "nie dizala";
        try {
            if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {


                String json = new String(new String(volleyError.networkResponse.data));

                Log.d(TAG, json);

                JSONObject input = new JSONObject(json);

                out = input.has("message") ? input.getString("message") : "";

                //Additional cases
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, out);
        return out;
    }

    public static double gps2m(float lat_a, float lng_a, float lat_b, float lng_b) {
        float pk = (float) (180 / 3.14169);

        float a1 = lat_a / pk;
        float a2 = lng_a / pk;
        float b1 = lat_b / pk;
        float b2 = lng_b / pk;

        float t1 = FloatMath.cos(a1) * FloatMath.cos(a2) * FloatMath.cos(b1) * FloatMath.cos(b2);
        float t2 = FloatMath.cos(a1) * FloatMath.sin(a2) * FloatMath.cos(b1) * FloatMath.sin(b2);
        float t3 = FloatMath.sin(a1) * FloatMath.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        double result = 6366000 * tt;
        return result;
    }

    public static void showMessageDialog(JSONObject data, final Context context, final BackgroundService backgroundService, final String to) {


        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(context).inflate(R.layout.dialog_message, null);
        final TextView tvMessage = (TextView) v.findViewById(R.id.tv_message);
        final EditText etMessage = (EditText) v.findViewById(R.id.et_msg);
        SmartPitSelectorView btnSend = (SmartPitSelectorView) v.findViewById(R.id.btn_send);
        int pad = (int) context.getResources().getDimension(R.dimen.btn_big_padding);
        btnSend.getImageView().setPadding(pad, pad, pad, pad);
        btnSend.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.send_message);

        String body = null;
        try {
            body = data.has("body") ? data.getString("body") : "";

            tvMessage.setText(body);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = etMessage.getText().toString();
                if (msg.trim().equals("")) {
                    Toast.makeText(context, "Podaj teść wiadomosći", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    backgroundService.emitMessage(to, msg);
                    tvMessage.append("\n" + msg);
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.rounded_bkg));
        dialog.setContentView(v);
        dialog.show();


    }

    public static void showNewOrderDialog(String message, final Context context) {


        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(context).inflate(R.layout.dialog_info, null);
        TextView textView=(TextView)v.findViewById(R.id.tv_body);
        textView.setText(message);
        dialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.rounded_bkg));
        dialog.setContentView(v);
        dialog.show();


    }

}
