package pl.slapps.motiv.motivrestaurant.background;

/**
 * Created by piotr on 01.05.15.
 */

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Parcel;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;


import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.motiv.motivrestaurant.R;


/**
 * Creates the head layer view which is displayed directly on window manager.
 * It means that this view is above every application's view on your phone -
 * until another application does the same.
 */
public class HeadLayer extends View {
    private String TAG = HeadLayer.class.getName();
    private Context context;
    private FrameLayout frameLayout;
    private WindowManager windowManager;

    public HeadLayer(Context context) {
        super(context);

        this.context = context;

        createHeadView(10, 10);
    }

    class MyDragListener implements OnDragListener {
        //Drawable enterShape = getResources().getDrawable(R.drawable.shape_droptarget);
        //Drawable normalShape = getResources().getDrawable(R.drawable.shape);

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();
            Log.d(TAG, "on drag action " + action);
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    //v.setBackgroundDrawable(enterShape);
                    break;
                case DragEvent.ACTION_DRAG_LOCATION:

                    Log.d(TAG, "Cur(X, Y) : " + event.getX() + ", " + event.getY());
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    Log.d(TAG, "on drag exited");

                    //v.setBackgroundDrawable(normalShape);
                    break;
                case DragEvent.ACTION_DROP:
                    // Dropped, reassign View to ViewGroup
                    Log.d(TAG, "on drop action");

                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //v.setBackgroundDrawable(normalShape);
                    Log.d(TAG, "drag ended");
                    View view = (View) event.getLocalState();
                    WindowManager.LayoutParams p=(WindowManager.LayoutParams)view.getLayoutParams();
                    float x = p.x;
                    float y = p.y;
                    //ViewGroup owner = windowManager;
                    windowManager.removeView(view);

                    //LinearLayout container = (LinearLayout) v;
                    createHeadView(x, y);
                    view.setVisibility(View.VISIBLE);
                default:
                    break;
            }
            return true;
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            ClipData data = ClipData.newPlainText("", "");
            DragShadowBuilder shadowBuilder = new DragShadowBuilder(view);
            view.startDrag(data, shadowBuilder, view, 0);
            view.setVisibility(View.INVISIBLE);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Creates head view and adds it to the window manager.
     */

    private void createHeadView(float x, float y) {
        Log.d(TAG, "create head view " + x + " " + y);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.LEFT|Gravity.TOP;


        params.y = (int) y;
        params.x = (int) x;

        frameLayout = new FrameLayout(context);
        //frameLayout.setLeft((int)x);
        //frameLayout.setTop((int)y);


        windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.addView(frameLayout, params);


        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // Here is the place where you can inject whatever layout you want.
        View lay = layoutInflater.inflate(R.layout.head, frameLayout);

       // SmartImageView image = (SmartImageView) lay.findViewById(R.id.iv_logo);
       // SmartPitAppHelper.getInstance(context).setImage(image, "http://i.iplsc.com/-/0003H785DIHVWY6U-C317-F3.jpg", 0, 0);
       /* lay.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return HeadLayer.this.onTouch(view, motionEvent);
            }
        });
*/
        ///frameLayout.setOnDragListener(new MyDragListener());
        lay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                destroy();
            }
        });


    }

    /**
     * Removes the view from window manager.
     */
    public void destroy() {
        windowManager.removeView(frameLayout);
    }
}