package pl.slapps.motiv.motivrestaurant.orders;

import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.background.BackgroundService;
import pl.slapps.motiv.motivrestaurant.model.Order;
import pl.slapps.motiv.motivrestaurant.widget.AppHelper;
import pl.slapps.motiv.motivrestaurant.widget.CustomSlidingPane;

/**
 * Created by piotr on 04.10.15.
 */
public class FragmentOrderDetails extends SmartPitFragment {

    private SmartPitSelectorView btnNav;
    private SmartPitSelectorView btnRoute;
    private SmartPitSelectorView btnMsg;
    private SmartPitSelectorView btnPromote;
    private TextView tvTitle;
    private TextView tvPhone;

    private TextView tvAddress;
    private TextView tvCoords;
    private TextView tvPrice;
    private TextView tvDesc;
    private LinearLayout layoutProducts;
    private BackgroundService backgroundService;

    public View getCartView(JSONObject element) {
        View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.row_order_detail_cart, null);
        TextView tvLabel = (TextView) v.findViewById(R.id.tv_name);
        TextView tvCount = (TextView) v.findViewById(R.id.tv_count);


        try {
            int count = element.has("count") ? element.getInt("count") : 0;
            JSONObject product = element.has("product") ? element.getJSONObject("product") : new JSONObject();
            String name = product.has("name") ? product.getString("name") : "";

            tvLabel.setText(name);
            tvCount.setText(count + "x ");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return v;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        backgroundService = ((MainActivity) this.getActivity()).getService();

        View v = inflater.inflate(R.layout.fragment_order_detail, parent, false);
        tvPhone = (TextView) v.findViewById(R.id.tv_phone);
        tvTitle = (TextView) v.findViewById(R.id.tv_title);
        tvAddress = (TextView) v.findViewById(R.id.tv_address);
        tvCoords = (TextView) v.findViewById(R.id.tv_coords);
        tvPrice = (TextView) v.findViewById(R.id.tv_price);
        tvDesc = (TextView) v.findViewById(R.id.tv_desc);
        layoutProducts = (LinearLayout) v.findViewById(R.id.cart_base);

        btnNav = (SmartPitSelectorView) v.findViewById(R.id.btn_nav);
        btnMsg = (SmartPitSelectorView) v.findViewById(R.id.btn_msg);

        btnRoute = (SmartPitSelectorView) v.findViewById(R.id.btn_route);
        btnPromote = (SmartPitSelectorView) v.findViewById(R.id.btn_promote);

        int padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);
        btnNav.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.navigate);
        btnNav.getImageView().setPadding(padding, padding, padding, padding);

        btnRoute.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.route);
        btnRoute.getImageView().setPadding(padding, padding, padding, padding);

        btnMsg.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.send_message);
        btnMsg.getImageView().setPadding(padding, padding, padding, padding);

        btnPromote.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.arrow_right);
        btnPromote.getImageView().setPadding(padding, padding, padding, padding);

        return v;
    }

    @Override
    public String getLabel() {
        return null;
    }

    public void initDetailView(final Order order) {


        //  label = order.address;
        setActionbarLabel();
        //pane.closePane();


        tvTitle.setText(order.title);
        tvAddress.setText(order.address);
        tvCoords.setText(order.latitude + " " + order.longitude);
        tvPrice.setText(order.price);
        tvPhone.setText(order.phone);
        tvDesc.setText(order.description);
        layoutProducts.removeAllViews();

        for (int i = 0; i < order.cart.length(); i++) {
            try {
                layoutProducts.addView(getCartView(order.cart.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        btnNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                       /*
                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + Double.toString(order.latitude) + "," + Double.toString(order.longitude));
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);
*/


                //  String uri = buildRequest();
                // Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                // startActivity(intent);
            }
        });

        btnRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //((MainActivity) FragmentOrderDetail.this.getActivity()).showProgressBar();
                //   if (fragmentMap != null)
                //        fragmentMap.toggleRoute(order);
                ((FragmentOrdersBase) FragmentOrderDetails.this.getParentFragment()).toggleRoute(order);

                CustomSlidingPane pane = ((FragmentOrdersBase) FragmentOrderDetails.this.getParentFragment()).getPaneTwo();
                if (pane != null)
                    pane.openPane();

            }
        });

        btnPromote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (backgroundService != null) {
                    String status = order.status;
                    if (status.equals("new"))
                        status = "production";
                    else if (status.equals("production"))
                        status = "to_deliver";
                    else if (status.equals("to_deliver"))
                        status = "on_way";
                    else if (status.equals("on_way"))
                        status = "done";

                    backgroundService.emitChangeStatus(order.id, status);

                    Toast.makeText(FragmentOrderDetails.this.getActivity(), "Status zamówienia został zmieniony na: " + status, Toast.LENGTH_LONG).show();
                }
            }
        });
        btnMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (backgroundService != null) {
                    AppHelper.showMessageDialog(new JSONObject(), FragmentOrderDetails.this.getActivity(), backgroundService, order.id);
                }
            }
        });

        ((FragmentOrdersBase) this.getParentFragment()).centerMarker(order);
        //fragmentMap.centerMarker(order);

        //  ((MainActivity) this.getActivity()).centerMarker(order);


    }
}