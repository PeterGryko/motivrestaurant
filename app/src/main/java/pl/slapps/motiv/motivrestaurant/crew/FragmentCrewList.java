package pl.slapps.motiv.motivrestaurant.crew;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.model.User;

/**
 * Created by piotr on 27.09.15.
 */
public class FragmentCrewList extends SmartPitFragment {

    private ListView listView;
    private SmartPitSelectorView btnAdd;

    private int padding;
    private ArrayList<User> list;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crew_list, parent, false);

        padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);

        listView = (ListView) v.findViewById(R.id.list_view);
        list = new ArrayList<>();

        for (int i = 0; i < MyApplication.currentCrew.length(); i++) {
            try {
                list.add(new User(MyApplication.currentCrew.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        listView.setAdapter(new AdapterCrew(this.getActivity(), list));

        btnAdd = (SmartPitSelectorView) v.findViewById(R.id.btn_add);

        btnAdd.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.plus);
        btnAdd.getImageView().setPadding(padding, padding, padding, padding);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((FragmentCrewBase) FragmentCrewList.this.getParentFragment()).switchFragment(new FragmentAddCrewMember());
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                switchDetails(list.get(i));

            }
        });

        if (list.size() > 0) {
            switchDetails(list.get(0));
        }

        return v;
    }

    private void switchDetails(User user) {
        Bundle arg = new Bundle();
        arg.putString("data", user.data.toString());
        FragmentCrewMember fragmentCrewMember = new FragmentCrewMember();
        fragmentCrewMember.setArguments(arg);

        ((FragmentCrewBase) FragmentCrewList.this.getParentFragment()).switchFragment(fragmentCrewMember);
    }

    @Override
    public String getLabel() {
        return null;
    }
}
