package pl.slapps.motiv.motivrestaurant.orders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 05.09.15.
 */
public class AdapterSummaryCart extends ArrayAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<JSONObject> data;


    public AdapterSummaryCart(Context context, ArrayList<JSONObject> data) {
        super(context, R.layout.row_order_summary, data);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.data = data;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        View v = inflater.inflate(R.layout.row_order_summary, null);
        TextView tvLabel = (TextView) v.findViewById(R.id.tv_name);
        TextView tvCount = (TextView) v.findViewById(R.id.tv_count);
        TextView tvPrice = (TextView) v.findViewById(R.id.tv_price);

        JSONObject element = data.get(position);


        try {
            int count = element.has("count") ? element.getInt("count") : 0;
            JSONObject product = element.has("product") ? element.getJSONObject("product") : new JSONObject();
            String name = product.has("name") ? product.getString("name") : "";
            JSONObject data = product.has("data") ? product.getJSONObject("data") : product;
            String price = data.has("price") ? data.getString("price") : "";

            tvLabel.setText(name);
            tvCount.setText(count + "x ");
            tvPrice.setText(price +"zł");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return v;
    }
}
