package pl.slapps.motiv.motivrestaurant.facebook;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.Response;
import com.android.volley.VolleyError;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import pl.gryko.smartpitlib.SmartPitActivity;
import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MyApplication;

/**
 * Created by piotr on 18.04.15.
 */
public class FacebookSynchronizer {

    private String TAG = FacebookSynchronizer.class.getName();
    private Context context;
    private SynchronizerListener synchListener;

    private HashMap<String, Object> tasksList;
    private int TASKS_COUNT;
    private int CURRENT_COUNT;

    private String SYNCING_PROFILE = "Synchronizacja danych użytkownika";
    private String SYNCING_PROFILE_IMAGE = "Synchronizacja zdjęcia profilowego";
    private String SYNCKING_GALLERY = "Synchronizacja galerii ";

    public FacebookSynchronizer(Context context, SynchronizerListener listener) {
        this.context = context;
        this.synchListener = listener;
        tasksList = new HashMap<>();
    }

    public void addTask(Object object) {
        tasksList.put(object.toString(), object);
    }

    public void removeTask(Object object) {
        tasksList.remove(object.toString());
        synchListener.onElementLoaded(object.toString());
    }

    public SynchronizerListener getSynchListener() {
        return synchListener;
    }


    public static interface SynchronizerListener {
        public void onElementLoaded(String key);


        public void onLabelChange(String label);

        ///public void onElementLaded(Strin key)

        public void onLoad(JSONObject object);
    }

    private void syncRestaurantGallery(final JSONObject data) {
        JSONArray media = null;

        synchListener.onLabelChange(SYNCKING_GALLERY);

        Log.d(TAG, "synch gallery");
        try {
            media = data.has("media") ? data.getJSONArray("media") : new JSONArray();
            Log.d(TAG, "synch gallery media size " + media.length());

            for (int i = 0; i < media.length(); i++) {
                addTask(media.getJSONObject(i));
            }
            TASKS_COUNT = media.length();
            CURRENT_COUNT = TASKS_COUNT;


            for (int i = 0; i < media.length(); i++) {


                final JSONObject mediaElement = media.getJSONObject(i);
                String path = mediaElement.has("path") ? mediaElement.getString("path") : "";
                //mediaElement.put("path", path);
                //media.put(mediaElement);


                final int index = i;
                SmartPitActivity.getImageLoader()
                        .get(path, new SmartPitImageLoader.SmartImagesListener() {
                            @Override
                            public void onResponse(SmartPitImageLoader.SmartImageContainer container, boolean flag) {
                                //  if (container.getBitmap() != null && !dataSynched)
                                //        uploadBitmapToServer(container.getBitmap(), context);
                                removeTask(mediaElement);
                                CURRENT_COUNT--;
                                synchListener.onLabelChange(SYNCKING_GALLERY + CURRENT_COUNT + "/" + TASKS_COUNT);
                                if (CURRENT_COUNT == 0)
                                    synchListener.onLoad(data);
                            }

                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }, 500, 500);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void uploadBitmapToServer(Bitmap b, final Object task, boolean logo) {
        FileOutputStream out = null;
        try {
            //  final Object tmp = new Object();
            // addTask(tmp);
            String path = context.getCacheDir() + "/" + b.toString();
            out = new FileOutputStream(path);
            b.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
            File file = new File(path);
            if (logo) {
            /*
                DAO.uploadRestaurantLogo(new Response.Listener() {
                    @Override
                    public void onResponse(Object o) {
                        Log.d(TAG, o.toString());
                        removeTask(task);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d(TAG, volleyError.toString());
                    }
                }, file);
            */
            } else {
              /*
                DAO.uploadRestaurantMedia(new Response.Listener() {
                    @Override
                    public void onResponse(Object o) {
                        Log.d(TAG, o.toString());
                        removeTask(task);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d(TAG, volleyError.toString());
                    }
                }, file);
            */
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void syncRestaurantGeo(final String _id, final JSONObject data) {
        final Object geoTask = new Object();
        //addTask(geoTask);

        synchListener.onLabelChange(SYNCING_PROFILE);

        JSONObject geojson = null;
        try {
            geojson = data.has("geojson") ? data.getJSONObject("geojson") : new JSONObject();


            DAO.setRestaurantNode(new Response.Listener() {
                @Override
                public void onResponse(Object o) {
                    Log.d(TAG, "set restaurant node " + o.toString());
                    //tasksList.remove(geojson)
                    removeTask(geoTask);

                    syncRestaurantData(_id, data);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d(TAG, volleyError.toString());
                }
            }, _id, "geojson", geojson);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void syncRestaurantData(String _id, final JSONObject data) {

        final Object personalTask = new Object();
        // addTask(personalTask);
        JSONObject personalData = null;
        try {
            personalData = data.has("data") ? data.getJSONObject("data") : new JSONObject();

            Log.d(TAG, " personal data " + personalData.toString());

            DAO.setRestaurantNode(new Response.Listener() {
                @Override
                public void onResponse(Object o) {
                    Log.d(TAG, o.toString());
                    Log.d(TAG, "set restaurant node " + o.toString());
                    removeTask(personalTask);

                    syncRestaurantLogo(data);


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d(TAG, volleyError.toString());

                }
            }, _id, "data", personalData);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void syncRestaurantLogo(final JSONObject data) {
        synchListener.onLabelChange(SYNCING_PROFILE_IMAGE);
        Log.d(TAG, "synch profile");
        try {
            final JSONObject logo = data.has("logo") ? data.getJSONObject("logo") : new JSONObject();

            addTask(logo);


            String path = logo.has("path") ? logo.getString("path") : "";
            SmartPitActivity.getImageLoader()
                    .get(path, new SmartPitImageLoader.SmartImagesListener() {
                        @Override
                        public void onResponse(SmartPitImageLoader.SmartImageContainer container, boolean flag) {
                            if (container.getBitmap() != null)
                                uploadBitmapToServer(container.getBitmap(), logo, true);
                            // removeTask(mediaElement);

                            syncRestaurantGallery(data);

                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }, 500, 500);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void synchronizeData(JSONObject data) {

        //Log.d(TAG,"synchronize daata "+data);
        try {
            String _id = MyApplication.currentRestaurant.id;

            JSONObject d = data.has("data") ? data.getJSONObject("data") : new JSONObject();
            Log.d(TAG, " beginning data " + d.toString());

            syncRestaurantGeo(_id, data);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
