package pl.slapps.motiv.motivrestaurant.facebook;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;


import pl.gryko.smartpitlib.widget.SmartPitAppDialog;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 20.04.15.
 */
public class FacebookSyncDialog {

    private AlertDialog dialog;
    private TextView tv;
    private TextView tvLabel;

    public FacebookSyncDialog(Context context) {
        dialog = SmartPitAppDialog.getAlertDialog(AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, context);
        View v = LayoutInflater.from(context).inflate(R.layout.layout_loading_dialog, null);
        tvLabel = (TextView) v.findViewById(R.id.tv_label);
        dialog.setView(v);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
    }

    public void showDialog() {

        dialog.show();
    }

    public void dismissDialog() {
        dialog.dismiss();

    }

    public void setLabel(String label) {
        if (tvLabel != null)
            tvLabel.setText(label);
    }


}
