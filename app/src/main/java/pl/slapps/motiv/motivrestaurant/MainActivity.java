package pl.slapps.motiv.motivrestaurant;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.IBinder;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.facebook.CallbackManager;

import com.facebook.FacebookSdk;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;


import java.io.File;

import java.lang.reflect.Field;

import android.os.Handler;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

import pl.gryko.smartpitlib.SmartPitNavigationDrawerActivity;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.motiv.motivrestaurant.background.BackgroundService;
import pl.slapps.motiv.motivrestaurant.fragment.FragmentGallery;
import pl.slapps.motiv.motivrestaurant.splash.FragmentSplash;
import pl.slapps.motiv.motivrestaurant.widget.AppHelper;


public class MainActivity extends SmartPitNavigationDrawerActivity {


    public interface OnImagePickListener {
        public void onPick(File file);
    }

    private OnImagePickListener pickListener;
    //private FragmentMap mapFragment;
    //private LinearLayout paneContent;
    //private CustomSlidingPane slidingPane;
    private FragmentManager fm;
    private String TAG = MainActivity.class.getName();
    private ActionBar ab;
    private ProgressBar abBar;
    private View abLayout;
    private ImageView backBtn;
    private TextView tvLabel;

    private CallbackManager callbackManager;

    private boolean areDriverRefreashing;
    private boolean areOrdersRefreashing;

    public GoogleApiClient mGoogleApiClient;

    private BackgroundService mService;
    private boolean mBound = false;
    private LinearLayout backBase;

    private Handler handler = new Handler();
    private Runnable newOrderTask = new Runnable() {
        @Override
        public void run() {
            AppHelper.showNewOrderDialog("Nowe zamówienie", MainActivity.this);
        }
    };


    //arg1 ==1 show progressbar
    //arg1 ==2 hide progressbar


    public void setPickListener(OnImagePickListener listener) {
        pickListener = listener;
    }

    public BackgroundService getService() {
        return mService;
    }


    public CallbackManager getCallbackManager() {
        return callbackManager;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        //CallbackManager.
        Log.d(TAG, "on activity result");
    }


    public void showActionbar() {
        ab.show();
    }

    private void initActionbar() {
        ab = this.getSupportActionBar();
        ab.setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));
        abLayout = LayoutInflater.from(this).inflate(R.layout.actionbar_layout, null);
        backBtn = (ImageView) abLayout.findViewById(R.id.back_btn);
        backBase = (LinearLayout) abLayout.findViewById(R.id.back_base);
        tvLabel = (TextView) abLayout.findViewById(R.id.tv_label);
        abBar = (ProgressBar) abLayout.findViewById(R.id.bar);
        abBar.setVisibility(View.GONE);
        ab.setDisplayShowCustomEnabled(true);
        ab.setDisplayHomeAsUpEnabled(false);
        ab.setDisplayHomeAsUpEnabled(false);
        ab.setDisplayShowTitleEnabled(false);
        ab.setDisplayUseLogoEnabled(false);
        ab.setCustomView(abLayout, new ActionBar.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));


        backBtn.setVisibility(View.GONE);
        Toolbar parent = (Toolbar) abLayout.getParent();
        parent.setContentInsetsAbsolute(0, 0);

        backBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "back pressed ");

                if (getManager().getBackStackEntryCount() > 0)
                    onBackPressed();
            }
        });
    }


    public void setActionBarLabel(String label) {
        Log.d(TAG, "set actionbar label " + label);
        if (tvLabel != null && label != null)
            tvLabel.setText(label);
    }

    public void onNewIntent(Intent i) {
        super.onNewIntent(i);
        Log.d(TAG, "on new intent");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Log.d(TAG, "on create " + this.toString());

        this.setDrawerGravity(Gravity.RIGHT);
        this.setDrawerFragment(new FragmentGallery());
        try {
            Field f = DrawerLayout.class.getDeclaredField("mMinDrawerMargin");
            f.setAccessible(true);
            f.set(getDrawerLayout(), 0);

            getDrawerLayout().requestLayout();
            getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        } catch (Exception e) {
            e.printStackTrace();
        }

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)


                .build();


        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();


        if (SmartPitAppHelper.isTablet(this)) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }


        initActionbar();


        fm = this.getSupportFragmentManager();
        this.setFirstFragment(new FragmentSplash());

        this.getManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getManager().getBackStackEntryCount() > 0)
                    backBtn.setVisibility(View.VISIBLE);
                else
                    backBtn.setVisibility(View.GONE);
            }
        });


    }


    public void onStart() {
        super.onStart();
        Log.d(TAG, "on start " + this.toString());
        mGoogleApiClient.connect();
        Intent intent = new Intent(this, BackgroundService.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);


    }

    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();

        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }

        Log.d(TAG, "on stop " + this.toString());
    }


    public void onExit() {


        AppHelper.showExitDialog(this);
    }


    public void switchFragmentWithoutAnimation(SmartPitFragment fragment, boolean removePrevious) {
        try {
            SmartPitFragment oldFragment = getCurrentFragment();

            fm.beginTransaction().remove(oldFragment)
                    .add(pl.gryko.smartpitlib.R.id.fragment_container, fragment).addToBackStack(null)
                    .commitAllowingStateLoss();

            setCurrentFragment(fragment, removePrevious);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void switchFragment(SmartPitFragment fragment, boolean flag) {
        if (this.getCurrentFragment().getClass() == fragment.getClass() && flag)
            return;
        else
            super.switchFragment(fragment, flag);

    }

    public void onCameraImagePicked(File file) {
        Log.d(TAG, " on camera image picked ");
        if (pickListener != null)
            pickListener.onPick(file);
    }

    public void onGalleryImagePicked(File file) {
        Log.d(TAG, " on gallery image picked ");
        if (pickListener != null)
            pickListener.onPick(file);
    }


    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            BackgroundService.LocalBinder binder = (BackgroundService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            Toast.makeText(MainActivity.this, "service connected", Toast.LENGTH_SHORT).show();

            mService.setOnMessage(new BackgroundService.OnMessageListener() {
                @Override
                public void onNewOrder(JSONObject result) {

                    handler.post(newOrderTask);
                }

                @Override
                public void onNewMessage(final JSONObject result) {

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            String from = null;
                            try {
                                from = result.has("from") ? result.getString("from") : "";

                                AppHelper.showMessageDialog(result, MainActivity.this, mService, from);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }

                @Override
                public void onUserJoin(final JSONObject result) {

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject who = result.has("who") ? result.getJSONObject("who") : new JSONObject();
                                JSONObject data = who.has("data") ? who.getJSONObject("data") : new JSONObject();
                                String name = data.has("name") ? data.getString("name") : "";
                                AppHelper.showNewOrderDialog(name + " rozpoczął pracę", MainActivity.this);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            mService = null;
            Toast.makeText(MainActivity.this, "service disconected", Toast.LENGTH_SHORT).show();

        }
    };


}
