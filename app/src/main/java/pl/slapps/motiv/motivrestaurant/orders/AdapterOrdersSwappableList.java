package pl.slapps.motiv.motivrestaurant.orders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import pl.gryko.smartpitlib.interfaces.SmartPitFragmentsInterface;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.draganddrop.DragNDropSimpleAdapter;
import pl.slapps.motiv.motivrestaurant.model.Order;

/**
 * Created by piotr on 07.05.15.
 */
public class AdapterOrdersSwappableList extends DragNDropSimpleAdapter{

    private String TAG = AdapterOrdersList.class.getName();
    private Context context;
    private ArrayList<Order> list;
    private SmartPitFragmentsInterface listener;
    private ListView lv;


    public AdapterOrdersSwappableList(Context context, ArrayList<Order> list, SmartPitFragmentsInterface listener, ListView lv, int handler) {
        super(context,  list,R.layout.row_in_progress_order,handler);
        //super(list);
        this.context = context;
        this.list = list;
        this.listener = listener;
        this.lv = lv;
    }




    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.row_in_progress_order, null);
        TextView tvAddress = (TextView) convertView.findViewById(R.id.tv_address);
        TextView tvCount = (TextView) convertView.findViewById(R.id.tv_products);
        TextView tvPrice = (TextView) convertView.findViewById(R.id.tv_price);

        LinearLayout baseLayout = (LinearLayout) convertView.findViewById(R.id.base_layout);
 /*
        baseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "on item click");
                Bundle arg = new Bundle();
                arg.putString("data", list.get(position).getJSONString());
                FragmentOrderDetail fd = new FragmentOrderDetail();
                fd.setArguments(arg);
                listener.switchFragment(fd, true);
            }
        });

      ImageView btnUp = (ImageView) convertView.findViewById(R.id.btn_up);
        ImageView btnDown = (ImageView) convertView.findViewById(R.id.btn_down);

        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position==list.size()-1)
                    return;
                Order object = list.get(position);
                list.remove(object);
                list.add(position+1,object);

                AdapterOrdersInProgress.this.notifyDataSetChanged();
                lv.invalidateViews();
                //AdapterOrdersInProgress.this.in
            }
        });

        btnDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position==0)
                    return;
                Order object = list.get(position);
                list.remove(object);
                list.add(position-1,object);

                AdapterOrdersInProgress.this.notifyDataSetChanged();
            }
        });
*/
        String address = ((Order)list.get(position)).address;
        String price = ((Order)list.get(position)).price;

        tvAddress.setText(address);
        tvPrice.setText(price);


        return convertView;

    }

    public ArrayList<Order> getList()
    {
        return list;
    }




}
