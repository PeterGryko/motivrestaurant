package pl.slapps.motiv.motivrestaurant.orders;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.crew.AdapterCrew;
import pl.slapps.motiv.motivrestaurant.crew.FragmentAddCrewMember;
import pl.slapps.motiv.motivrestaurant.crew.FragmentCrewBase;
import pl.slapps.motiv.motivrestaurant.crew.FragmentCrewMember;
import pl.slapps.motiv.motivrestaurant.model.User;

/**
 * Created by piotr on 04.10.15.
 */
public class FragmentNewOrderList extends SmartPitFragment {

    private String TAG  = FragmentNewOrderList.class.getName();
    private AdapterOrderProduct adapter;
    private SmartPitSelectorView btnNext;
    private ListView lv;
    private ArrayList<JSONObject> listData;

    private TextView tvPriceSum;
    private TextView tvCountSum;

    private void calculateSummary() {

        double priceSum = 0;
        int countSum = 0;

        for (int i = 0; i < listData.size(); i++) {
            try {
                JSONObject element = listData.get(i);
                JSONObject product = element.has("product") ? element.getJSONObject("product") : new JSONObject();
                JSONObject data = product.has("data") ? product.getJSONObject("data") : new JSONObject();

                int count = element.has("count") ? element.getInt("count") : 0;
                countSum += count;

                double price = data.has("price") ? data.getDouble("price") : 0;
                Log.d(TAG, "price " + price);
                priceSum += (count > 0 ? count * price : price);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
        tvCountSum.setText(Integer.toString(countSum));
        tvPriceSum.setText(priceSum + "zł");
    }

    public void updateListData(JSONObject data) {

        boolean added = false;
        for (int i = 0; i < listData.size(); i++) {
            try {
                JSONObject element = listData.get(i);
                JSONObject product = element.has("product") ? element.getJSONObject("product") : new JSONObject();
                int count = element.has("count") ? element.getInt("count") : 1;

                if (product == data) {
                    count++;
                    element.put("count", count);
                    added = true;
                    break;
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }


        }
        if (!added) {
            try {
                JSONObject element = new JSONObject();
                element.put("count", 1);
                element.put("product", data);
                listData.add(element);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
        if (lv.getAdapter() == null) {
            adapter = new AdapterOrderProduct(this.getActivity(), listData);
            lv.setAdapter(adapter);
        } else
            adapter.notifyDataSetChanged();

        calculateSummary();
    }

    private void popListData(int position) {
        JSONObject element = listData.get(position);
        try {
            int count = element.has("count") ? element.getInt("count") : 0;

            if (count > 1) {
                count--;
                element.put("count", count);
            } else {
                listData.remove(position);

            }
            adapter.notifyDataSetChanged();


        } catch (JSONException e) {
            e.printStackTrace();
        }

        calculateSummary();
    }



    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_new_order_list, parent, false);

        lv = (ListView) v.findViewById(R.id.lv);

        tvCountSum = (TextView) v.findViewById(R.id.tv_count_sum);
        tvPriceSum = (TextView) v.findViewById(R.id.tv_price_sum);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                popListData(i);
            }
        });
        listData = new ArrayList<>();


        btnNext = (SmartPitSelectorView) v.findViewById(R.id.btn_next);
        btnNext.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.cart);
        int padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);
        btnNext.getImageView().setPadding(padding, padding, padding, padding);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                JSONArray array = new JSONArray();

                for (int i = 0; i < listData.size(); i++) {
                    array.put(listData.get(i));
                }
                Bundle arg = new Bundle();
                arg.putString("data", array.toString());
                arg.putString("price", tvPriceSum.getText().toString());

                FragmentNewOrderType ft = new FragmentNewOrderType();
                ft.setArguments(arg);
                ((MainActivity) FragmentNewOrderList.this.getActivity()).switchFragment(ft, true);
            }
        });

        return v;
    }

    @Override
    public String getLabel() {
        return null;
    }
}
