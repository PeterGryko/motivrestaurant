package pl.slapps.motiv.motivrestaurant.placemenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 02.09.15.
 */
public class AdapterMenuSlider extends ArrayAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<JSONObject> data;
    private String active;

    public void setActiveRow(String id)
    {

        active=id;
    }

    public AdapterMenuSlider(Context context, ArrayList<JSONObject> data) {
        super(context, R.layout.row_menu_slider, data);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public String getItemID(int position)
    {
        JSONObject element = data.get(position);
        String id = null;
        try {
            id = element.has("_id")?element.getString("_id"):"";
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return id;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = inflater.inflate(R.layout.row_menu_slider, null);
        TextView label = (TextView) v.findViewById(R.id.tv_label);
        JSONObject element = data.get(position);
        ListTriangle triangle = (ListTriangle) v.findViewById(R.id.triangle);
        triangle.setVisibility(View.GONE);

        try {
            String name = element.has("label") ? element.getString("label") : "";

            String id = element.has("_id") ? element.getString("_id") : "";

            label.setText(name);

            if (active==null || active.equals(id))
                triangle.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return v;
    }
}
