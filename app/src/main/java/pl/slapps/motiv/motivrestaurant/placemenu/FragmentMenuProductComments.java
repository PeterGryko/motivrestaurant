package pl.slapps.motiv.motivrestaurant.placemenu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 14.09.15.
 */
public class FragmentMenuProductComments extends SmartPitFragment{

    public View onCreateView(LayoutInflater inflater,ViewGroup parent, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_menu_product_comments,parent,false);

        return v;
    }
    @Override
    public String getLabel() {
        return null;
    }
}
