package pl.slapps.motiv.motivrestaurant.placemenu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 16.09.15.
 */
public class FragmentMenuProductsList extends SmartPitFragment {

    private String TAG = FragmentMenuProductsList.class.getName();

    private ListView lv;
    private AdapterMenuProducts adapter;
    private JSONObject data;
    private FragmentMenuTree host;
    private SmartPitSelectorView btnAdd;

    public void setHost(FragmentMenuTree host) {
        this.host = host;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_menu_products_list, parent, false);

        lv = (ListView) v.findViewById(R.id.lv);

        btnAdd = (SmartPitSelectorView) v.findViewById(R.id.btn_add_product);

        btnAdd.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.plus);
        int padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);
        btnAdd.getImageView().setPadding(padding, padding, padding, padding);




        if (this.getArguments() != null) {
            try {
                data = new JSONObject(this.getArguments().getString("parent"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }



        if (data == null) {
            /// btn_add_product.setVisibility(View.GONE);
            initRootProducts();
        } else {

            initChildProducts();

        }

        return v;
    }

    private void initRootProducts() {
        DAO.getProducts(new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());
                parseHttpResponse(o.toString(), (MainActivity) FragmentMenuProductsList.this.getActivity(), data);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Log.d(TAG, volleyError.toString());
            }
        }, MyApplication.currentRestaurant.id, null);
        btnAdd.setVisibility(View.GONE);
    }

    private void initChildProducts() {
        try {
            String id = data.has("_id") ? data.getString("_id") : "";

            DAO.getProducts(new Response.Listener() {
                @Override
                public void onResponse(Object o) {
                    Log.d(TAG, o.toString());
                    parseHttpResponse(o.toString(), (MainActivity) FragmentMenuProductsList.this.getActivity(), data);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                    Log.d(TAG, volleyError.toString());
                }
            }, MyApplication.currentRestaurant.id, id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                host.initAddProductPanel();
            }
        });



    }


    private void parseHttpResponse(String o, final MainActivity context, final JSONObject category) {
        try {
            JSONObject result = new JSONObject(o.toString());
            result = result.has("api") ? result.getJSONObject("api") : new JSONObject();


            final JSONArray data = result.has("results") ? result.getJSONArray("results") : new JSONArray();

            final ArrayList<JSONObject> objects = new ArrayList<>();

            for (int i = 0; i < data.length(); i++) {
                JSONObject d = data.getJSONObject(i);
                objects.add(d);

            }

            if (lv != null && FragmentMenuProductsList.this.isAdded()) {

                adapter = new AdapterMenuProducts(context, objects);
                lv.setAdapter(adapter);

                if(objects.size()>0)
                {
                    adapter.setActive(adapter.getItemID(0));
                    adapter.notifyDataSetChanged();
                    host.refreashProductPanel(objects.get(0));
                }

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        //  host.closePane();
                        //  host.closeChildPane();
                        adapter.setActive(adapter.getItemID(i));
                        adapter.notifyDataSetChanged();
                        host.refreashProductPanel(objects.get(i));
                        // initView(objects.get(i));

                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getLabel() {
        return null;
    }
}
