package pl.slapps.motiv.motivrestaurant.adapter;

/**
 * Created by piotr on 16.03.15.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import java.util.ArrayList;


/*

Flipping images gallery. Simple ViewPager adapter witch images.
Constructor parameters:
context, list of images urls, required width and height of image.
Images are loaded asynchronously. While data are feched, default android progressBar is visible.

 */

public class AdapterPager extends PagerAdapter {

    private String TAG = AdapterPager.class.getName();
    private ArrayList<View> list;
    private Context context;

    public AdapterPager(Context context,
                        ArrayList<View> list) {

        this.context = context;
        this.list = list;


    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //SmartPitAppHelper.getInstance(context).stripViewGroup((RelativeLayout) object, true);

        ((ViewPager) container).removeView((RelativeLayout) object);

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View row = list.get(position);

        ((ViewPager) container).addView(row, 0);


        return list.get(position);

    }

}
