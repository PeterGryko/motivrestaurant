package pl.slapps.motiv.motivrestaurant.facebook;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;


import java.util.ArrayList;

import pl.gryko.smartpitlib.SmartPitActivity;
import pl.gryko.smartpitlib.widget.Log;

/**
 * Created by piotr on 18.04.15.
 */
public class Http {
    //private static final String ACCESS_TOKEN = "CAACEdEose0cBAOO2EdCY1SjZCdS12AZBGGrn7ZCawhDKNMQHEdbhf06nr7ax3tYiP5qv0ZCmBOChk6CLm07AlnBC3TGSDNwXgWaZB8B8HraiVsOF5A8rfMZASuTLZAuQWg8PZB3mzYgHNEZBsJRfS0YUCZC6HcvCYq643bZARSLJETlE47I1XKQC8hz2UmEMpMBIUb0jdaptYEwQveUoFhtRkgF";

    private static int MY_TIMEOUT = 100 * 1000;
    private static String TAG = Http.class.getName();

    public static void getFacebookProfileDetails(Response.Listener listener, Response.ErrorListener errorListener, String id, String token) {

        String request =
                "https://graph.facebook.com/v2.3/" + id + "?access_token=" + token;
        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }

    public static void getUploadedImages(Response.Listener listener, Response.ErrorListener errorListener, String id, String token) {

        String request =
                "https://graph.facebook.com/v2.3/" + id + "/photos/uploaded?access_token=" + token;
        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }

    public static void getProfileImage(Response.Listener listener, Response.ErrorListener errorListener, String id, String token) {

        String request =
                "https://graph.facebook.com/v2.3/" + id + "/picture?type=large&redirect=false&access_token=" + token;
        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }

    public static void getProfilePages(Response.Listener listener, Response.ErrorListener errorListener, String id, String token) {

        String request =
                "https://graph.facebook.com/v2.3/me/accounts?access_token=" + token;
        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }

    public static void getBaseData(Response.Listener listener, Response.ErrorListener errorListener, ArrayList<String> ids, String token) {

        String id = "";

        for (int i = 0; i < ids.size(); i++) {
            id += ids.get(i);
            if (i < ids.size() - 1)
                id += ",";
        }
        String request =
                "https://graph.facebook.com/v2.3/?ids=" + id + "&access_token=" + token;
        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }
}
