package pl.slapps.motiv.motivrestaurant.placemenu;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by piotr on 02.09.15.
 */
public class ListTriangle extends View {

    public Paint paint;

    private void init()
    {
        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);

    }

    public ListTriangle(Context context) {
        super(context);
        init();
    }

    public ListTriangle(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public ListTriangle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();

    }
    public Paint getPaint()
    {
        return paint;
    }

    public void onDraw(Canvas canvas)
    {

        float[] points = new float[]{
                0,this.getHeight()/2,
                this.getWidth(),0,
                this.getWidth(),this.getHeight(),
                0,this.getHeight()/2

        };


        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(points[0], points[1]);
        path.lineTo(points[2], points[3]);
        path.lineTo(points[4], points[5]);
        path.lineTo(points[6], points[7]);
        path.close();

        canvas.drawPath(path, paint);
    }
}
