package pl.slapps.motiv.motivrestaurant.orders;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 04.10.15.
 */
public class FragmentNewOrderGrid extends SmartPitFragment{

    private String TAG = FragmentNewOrderGrid.class.getName();
    private GridView gridView;
    private FragmentNewOrderBase host;

    public void setHost(FragmentNewOrderBase host)
    {
        this.host=host;
    }

    private void initGridView(final ArrayList<JSONObject> objects) {
        gridView.setAdapter(new AdapterProductsGrid(FragmentNewOrderGrid.this.getActivity(), objects));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(host!=null)
                host.updateListData(objects.get(i));
            }
        });
    }


    @Override
    public String getLabel() {
        return null;
    }
    public View onCreateView(LayoutInflater inflater,ViewGroup parent, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_new_order_grid,parent,false);

        gridView = (GridView) v.findViewById(R.id.grid_view);

        DAO.getProducts(new Response.Listener() {
                            @Override
                            public void onResponse(Object o) {
                                Log.d(TAG, o.toString());
                                final JSONArray data;
                                try {
                                    JSONObject r = new JSONObject(o.toString());
                                    JSONObject api = r.has("api") ? r.getJSONObject("api") : new JSONObject();
                                    data = api.has("results") ? api.getJSONArray("results") : new JSONArray();


                                    final ArrayList<JSONObject> objects = new ArrayList<>();

                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject d = data.getJSONObject(i);
                                        objects.add(d);


                                    }
                                    initGridView(objects);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                //parseHttpResponse(o.toString(), (MainActivity) FragmentMenuProducts.this.getActivity(), data);
                            }
                        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        Log.d(TAG, volleyError.toString());
                    }
                }, MyApplication.currentRestaurant.id, null);

        return v;
    }
}
