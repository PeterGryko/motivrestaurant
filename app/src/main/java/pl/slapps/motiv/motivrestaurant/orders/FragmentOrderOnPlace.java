package pl.slapps.motiv.motivrestaurant.orders;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.fragment.FragmentMainMenu;

/**
 * Created by piotr on 05.09.15.
 */
public class FragmentOrderOnPlace extends SmartPitFragment {

    private String TAG = FragmentOrderOnPlace.class.getName();
    private SmartPitSelectorView buy;

    private ListView lv;
    private AdapterSummaryCart adapter;
    private ArrayList<JSONObject> dataList;
    private TextView tvPriceSum;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_on_place, parent, false);

        tvPriceSum = (TextView) v.findViewById(R.id.tv_price_sum);
        buy = (SmartPitSelectorView) v.findViewById(R.id.bottom);

        int padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);
        buy.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.checkin);
        buy.getImageView().setPadding(padding, padding, padding, padding);

        lv = (ListView) v.findViewById(R.id.lv);
        dataList = new ArrayList<>();
        if (this.getArguments() != null) {
            try {
                final String price = this.getArguments().getString("price");
                tvPriceSum.setText(price);

                final JSONArray array = new JSONArray(this.getArguments().getString("data"));
                for (int i = 0; i < array.length(); i++) {
                    dataList.add(array.getJSONObject(i));
                }
                adapter = new AdapterSummaryCart(this.getActivity(), dataList);
                lv.setAdapter(adapter);


                buy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        JSONObject data = new JSONObject();
                        try {
                            data.put("cart", array);
                            data.put("owner", MyApplication.currentRestaurant.id);
                            JSONObject details = new JSONObject();
                            details.put("on_place", true);
                            details.put("price", price);
                            details.put("lat", MyApplication.currentRestaurant.lat);
                            details.put("lon", MyApplication.currentRestaurant.lon);

                            data.put("deliveryData", details);

                            DAO.createOrder(data, new Response.Listener() {
                                @Override
                                public void onResponse(Object o) {

                                    Log.d(TAG, o.toString());


                                    ((MainActivity) FragmentOrderOnPlace.this.getActivity()).switchTitleFragment(new FragmentMainMenu(), true);
                                    Toast.makeText(FragmentOrderOnPlace.this.getActivity(), "Zamówienie zostalo złożone", Toast.LENGTH_LONG).show();
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {
                                    Log.d(TAG, volleyError.toString());
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return v;
    }

    @Override
    public String getLabel() {
        return null;
    }
}
