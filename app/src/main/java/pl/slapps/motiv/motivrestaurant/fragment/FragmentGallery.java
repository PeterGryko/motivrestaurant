package pl.slapps.motiv.motivrestaurant.fragment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.adapter.SmartPitImagesFlipperAdapter;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 02.04.15.
 */
public class FragmentGallery extends SmartPitFragment {
    private ViewPager pager;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_gallery, parent, false);
        pager = (ViewPager) v.findViewById(R.id.pager);

        if (this.getArguments() != null) {

            refreashPager(this.getArguments().getString("data"),0);

        }


        return v;
    }

    public void refreashPager(String data, int current) {
        try {
            JSONArray array = new JSONArray(data);
            ArrayList<String> images = new ArrayList<>();


            for (int i = 0; i < array.length(); i++) {
               // JSONObject element = array.getJSONObject(i);
               /// String path = element.has("path") ? element.getString("path") : "";
                images.add(array.getString(i));
            }

            pager.setAdapter(new SmartPitImagesFlipperAdapter(this.getActivity(), images, 0, 0));

            pager.setCurrentItem(current);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getLabel() {
        return null;
    }
}
