package pl.slapps.motiv.motivrestaurant.splash;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.widget.AppHelper;

/**
 * Created by piotr on 27.06.15.
 */
public class FragmentAddPlace extends SmartPitFragment {

    private EditText searchbar;
    private ListView lv;
    private AdapterPlaces adapter;

    private TextView tv_label;

    private String TAG = FragmentAddPlace.class.getName();

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstaceState) {
        View v = inflater.inflate(R.layout.fragment_add_place, parent, false);

        searchbar = (EditText) v.findViewById(R.id.searchbar);

        lv = (ListView) v.findViewById(R.id.lv);
        tv_label = (TextView) v.findViewById(R.id.tv_label);
        adapter = new AdapterPlaces((MainActivity) this.getActivity(), tv_label);

        lv.setAdapter(adapter);


        searchbar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                adapter.getFilter().filter(editable.toString());
            }
        });


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {

                DAO.getRestaurantData(new Response.Listener() {
                    @Override
                    public void onResponse(Object o) {

                        Log.d(TAG, o.toString());

                        JSONObject data = null;
                        try {
                            data = new JSONObject(o.toString());
                            JSONObject place = data.has("place") ? data.getJSONObject("place") : new JSONObject();
                            String id = place.has("_id") ? place.getString("_id") : "";
                            createPlace(id);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d(TAG, volleyError.toString());

                    }
                }, adapter.getPoint(i).id);
            }
        });
        return v;

    }

    private void createPlace(String placeId) {

        JSONObject data = new JSONObject();
        try {
            data.put("place", placeId);

            JSONArray crew = new JSONArray();
            crew.put(MyApplication.getTokenUserId());
            data.put("crew", crew);
        } catch (JSONException e) {
            e.printStackTrace();
        }




        DAO.createPlace(new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());

                JSONObject data = null;
                try {
                    data = new JSONObject(o.toString());

                    String id = data.has("_id") ? data.getString("_id") : "";

                   // setPLace(place, id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, volleyError.toString());

                AppHelper.getErrorMessage(volleyError);
            }
        },data);
    }

    /*
    private void setPLace(String id, String restaurant) {

        DAO.setPlace(id, restaurant, new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, volleyError.toString());

                AppHelper.getErrorMessage(volleyError);
            }
        });
    }
*/
    @Override
    public String getLabel() {
        return null;
    }
}
