package pl.slapps.motiv.motivrestaurant.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 26.09.15.
 */
public class AdapterGridGallery extends BaseAdapter {

    private Drawable mock;
    private Context context;
    private ArrayList<String> data;

    public AdapterGridGallery(Context context, ArrayList<String> data) {
        this.context = context;
        this.data = data;
        mock = context.getResources().getDrawable(R.drawable.black_food);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    class ViewHolder {
        public SmartImageView imageView;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.row_menu_product_gallery, null);
            SmartImageView imageView = (SmartImageView) view.findViewById(R.id.image);
            imageView.setMode(SmartImageView.Mode.NORMAL.ordinal());
            imageView.getImageView().setScaleType(ImageView.ScaleType.CENTER_CROP);


            SmartPitImageLoader.setImage(context, imageView, data.get(i), 0, 0);

          
        }

        return view;
    }
}
