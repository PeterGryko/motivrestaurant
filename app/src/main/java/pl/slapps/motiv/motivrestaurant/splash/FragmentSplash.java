package pl.slapps.motiv.motivrestaurant.splash;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;


import org.json.JSONException;
import org.json.JSONObject;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 16.03.15.
 */
public class FragmentSplash extends SmartPitFragment {
    private String TAG = FragmentSplash.class.getName();
    private SmartPitSelectorView btnLogin;
    private EditText etLogin;
    private EditText etPassword;

    private ImageView logo;

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_splash, parent, false);

        btnLogin = (SmartPitSelectorView) v.findViewById(R.id.btn_login);
        etLogin = (EditText) v.findViewById(R.id.et_login);
        etPassword = (EditText) v.findViewById(R.id.et_password);
        logo = (ImageView) v.findViewById(R.id.iv_logo);

        String login = SmartPitAppHelper.getPreferences(this.getActivity()).getString("login", "");
        String password = SmartPitAppHelper.getPreferences(this.getActivity()).getString("password", "");

        etLogin.setText(login);
        etPassword.setText(password);

        btnLogin.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.login);
        int padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);
        btnLogin.getImageView().setPadding(padding, padding, padding, padding);


        btnLogin.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                //((MainActivity) FragmentSplash.this.getActivity()).switchTitleFragment(new FragmentMainMenu(), true);

                final String login = etLogin.getText().toString();
                final String password = etPassword.getText().toString();

                logo.setVisibility(View.GONE);
                btnLogin.setEnabled(false);


                if (login.equals("") || password.equals(""))

                {
                    Toast.makeText(FragmentSplash.this.getActivity(), "Podaj login oraz hasło", Toast.LENGTH_LONG).show();
                    return;
                }
                JSONObject data = new JSONObject();
                try {

                    data.put("username", login);
                    data.put("password", password);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                DAO.loginUser(data, new Response.Listener() {
                    @Override
                    public void onResponse(Object o) {
                        Log.d(TAG, o.toString());


                        JSONObject data = null;
                        try {
                            data = new JSONObject(o.toString());

                            MyApplication.token = data.has("token") ? data.getString("token") : "";


                            Log.d(TAG, MyApplication.getTokenUserId().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        SmartPitAppHelper.getPreferences(FragmentSplash.this.getActivity()).edit().putString("login", login).commit();
                        SmartPitAppHelper.getPreferences(FragmentSplash.this.getActivity()).edit().putString("password", password).commit();

                        FragmentSplash.this.getFragmentsListener().switchFragment(new FragmentMyPlaces(), true);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d(TAG, volleyError.toString());
                        logo.setVisibility(View.VISIBLE);
                        btnLogin.setEnabled(true);
                        Toast.makeText(FragmentSplash.this.getActivity(), "Złe kredencje", Toast.LENGTH_LONG).show();
                    }
                });
                /*
                ((MainActivity) FragmentSplash.this.getActivity()).refreashRestaurantData(new Response.Listener() {
                    @Override
                    public void onResponse(Object o) {


                        Log.d(TAG, o.toString());
                        FragmentSplash.this.getFragmentsListener().switchTitleFragment(new FragmentLists(), true);

                        //parseJSONResponse(o.toString(), view, inflater, true);
                        //bar.setVisibility(View.GONE);
                    }

                });

*/
            }

        });
        return v;
    }

    @Override
    public String getLabel() {
        return "Logowanie";
    }
}
