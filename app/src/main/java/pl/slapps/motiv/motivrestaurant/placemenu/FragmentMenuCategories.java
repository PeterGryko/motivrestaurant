package pl.slapps.motiv.motivrestaurant.placemenu;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.background.BackgroundService;

/**
 * Created by piotr on 01.09.15.
 */
public class FragmentMenuCategories extends SmartPitFragment {

    private ListView lv;
    // private ArrayList<String> dataList;
    private SmartPitSelectorView btnAdd;
    //private EditText etName;
    private ArrayList<JSONObject> jsonList;

    //private LinearLayout add_product;
    private String TAG = FragmentMenuCategories.class.getName();


    private TextView tv_label;


    public void setHost(FragmentMenuTree host) {
        //   this.host = host;
    }

    public void showNewCategoryDialog(final Context context, final String id) {


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View v = LayoutInflater.from(context).inflate(R.layout.dialog_add_menu_element, null);
        final EditText etName = (EditText) v.findViewById(R.id.et_name);
        SmartPitSelectorView btnSend = (SmartPitSelectorView) v.findViewById(R.id.btn_add);
        int pad = (int) context.getResources().getDimension(R.dimen.btn_big_padding);
        btnSend.getImageView().setPadding(pad, pad, pad, pad);
        btnSend.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.plus);


        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etName.getText().toString().equals("")) {
                    Toast.makeText((MainActivity) FragmentMenuCategories.this.getActivity(), "wpisz nazwe kategori", Toast.LENGTH_LONG).show();
                    return;
                }
                DAO.addCategory(new Response.Listener() {
                    @Override
                    public void onResponse(Object o) {

                        Log.d(TAG, o.toString());
                        refreash(id, (MainActivity) FragmentMenuCategories.this.getActivity());

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d(TAG, volleyError.toString());
                    }
                }, MyApplication.currentRestaurant.id, id, etName.getText().toString());
                dialog.hide();
            }
        });
        dialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.rounded_bkg));
        dialog.setContentView(v);
        dialog.show();


    }


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_menu_categories, null);
        lv = (ListView) v.findViewById(R.id.list_view);
        btnAdd = (SmartPitSelectorView) v.findViewById(R.id.btn_add);
        btnAdd.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.plus);
        int padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);
        btnAdd.getImageView().setPadding(padding, padding, padding, padding);
        // etName = (EditText) v.findViewById(R.id.et_name);

        tv_label = (TextView) v.findViewById(R.id.tv_label);

        //add_product = (LinearLayout) v.findViewById(R.id.btn_add_product);


        jsonList = new ArrayList<>();


        Log.d(TAG, "on create view ");


        if (this.getArguments() != null) {


            try {

                final JSONObject p = new JSONObject(this.getArguments().getString("parent"));

                final String id = p.has("_id") ? p.getString("_id") : "";
                String label = p.has("label") ? p.getString("label") : "";
                tv_label.setText(label);

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Bundle arg = new Bundle();

                        JSONObject element = jsonList.get(i);

                        arg.putString("parent", element.toString());
                        arg.putString("owner", p.toString());

                        FragmentMenuTree fc = new FragmentMenuTree();
                        fc.setArguments(arg);
                        ((MainActivity) FragmentMenuCategories.this.getActivity()).switchFragment(fc, false);

                    }
                });


                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        showNewCategoryDialog(FragmentMenuCategories.this.getActivity(), id);
                    }

                });

                refreash(id, (MainActivity) FragmentMenuCategories.this.getActivity());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

            Log.d(TAG,"btn add ");
            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showNewCategoryDialog(FragmentMenuCategories.this.getActivity(), null);
                }

            });

/*
            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String cat = etName.getText().toString();

                    if (cat.trim().equals(""))
                        return;

                    DAO.addCategory(new Response.Listener() {
                        @Override
                        public void onResponse(Object o) {
                            Log.d(TAG, o.toString());
                            refreash(null, FragmentMenuCategories.this.getActivity());
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.d(TAG, volleyError.toString());
                        }
                    }, MyApplication.currentRestaurant.id, null, cat);
                }
            });

*/
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Bundle arg = new Bundle();

                    JSONObject element = jsonList.get(i);
                    // String id = null;

//                    id = element.has("_id") ? element.getString("_id") : "";

                    arg.putString("parent", element.toString());
                    FragmentMenuTree fm = new FragmentMenuTree();
                    fm.setArguments(arg);
                    ((MainActivity) FragmentMenuCategories.this.getActivity()).switchFragment(fm, false);

                }
            });

            refreash(null, (MainActivity) FragmentMenuCategories.this.getActivity());
        }
        return v;
    }

    private void refreash(String id, final Context context) {
        //  dataList = new ArrayList<>();

        DAO.getCategories(MyApplication.currentRestaurant.id, new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());
                JSONArray array = null;
                try {

                    jsonList.clear();
                    JSONObject result = new JSONObject(o.toString());
                    result = result.has("api") ? result.getJSONObject("api") : new JSONObject();

                    array = result.has("getChildren") ? result.getJSONArray("getChildren") : (result.has("results") ? result.getJSONArray("results") : new JSONArray());

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject element = array.getJSONObject(i);
                        jsonList.add(element);

                        //  String label = element.has("label") ? element.getString("label") : "";
                        //  dataList.add(label);

                    }
                    //if (FragmentRestaurantChildCategories.this.isAdded())
                    lv.setAdapter(new AdapterMenuCategories(context, jsonList));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, volleyError.toString());
            }
        }, id);
    }

    @Override
    public String getLabel() {
        return null;
    }
}
