package pl.slapps.motiv.motivrestaurant.splash;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.widget.RatingWidget;

/**
 * Created by piotr on 27.06.15.
 */
public class AdapterMyPlaces extends ArrayAdapter {

    private ArrayList<JSONObject> list;
    private Context context;

    public AdapterMyPlaces(Context context, ArrayList<JSONObject> list) {
        super(context, android.R.layout.simple_list_item_1, list);

        this.context = context;
        this.list = list;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.row_my_place, null);

        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
        TextView tvStreet = (TextView) convertView.findViewById(R.id.tv_street);
        SmartImageView avatar = (SmartImageView) convertView.findViewById(R.id.avatar);
        RatingWidget ratingWidget = (RatingWidget) convertView.findViewById(R.id.rating);

        JSONObject data = list.get(position);
        String name = "";
        String address = "";
        try {
            data = data.has("place") ? data.getJSONObject("place") : data;
            address = data.has("formatted_address") ? data.getString("formatted_address") : "";
            JSONObject google = data.has("google") ? data.getJSONObject("google") : data;
            String url = google.has("avatar") ? google.getString("avatar") : "";
            double rating = google.has("rating") ? google.getDouble("rating") : 0;


            data = data.has("data") ? data.getJSONObject("data") : data;
            name = data.has("name") ? data.getString("name") : "";

            tvName.setText(name);
            tvStreet.setText(address);
            ratingWidget.setScore((int) rating);
            if (!url.equals(""))
                SmartPitImageLoader.setImage(context, avatar, url, 0, 0);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return convertView;
    }

    public JSONObject getPoint(int pos) {
        return list.get(pos);
    }

    public String getPointId(int pos) {

        String id = "";
        JSONObject data = list.get(pos);

        try {
            data = data.has("place") ? data.getJSONObject("place") : new JSONObject();

            id = data.has("_id") ? data.getString("_id") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return id;
    }

    public String getRestaurantId(int pos) {

        String id = "";
        JSONObject data = list.get(pos);

        try {

            id = data.has("_id") ? data.getString("_id") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return id;
    }


}
