package pl.slapps.motiv.motivrestaurant.placemenu;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.widget.AppHelper;

/**
 * Created by piotr on 01.09.15.
 */
public class FragmentMenuProduct extends SmartPitFragment {

    private String TAG = FragmentMenuProduct.class.getName();
    // private LinearLayout btn_add_product;
    private FragmentMenuTree host;
    private JSONObject productData;
    private int padding;
    private int paddingBig;


    private TextView btn_add_ingredient;
    private LinearLayout ingredients;
    private ArrayList<Ingredient> ingredientsList;
   // private EditText et_ingredient;
    private TextView tv_category;

    private SmartImageView ivImage;
    private EditText et_name;
    private EditText et_price;
    private EditText et_desc;

    private SmartPitSelectorView btn_confirm;
    private JSONObject parentCategory;
    private JSONObject currentProduct;


    private SmartPitSelectorView btnComments;
    private SmartPitSelectorView btnGallery;


  //  private SmartPitSelectorView btnEdit;

    private ShowingAnimation etNameAnimation;
    private ShowingAnimation etDescAnimation;
    private ShowingAnimation etPriceAnimation;


    class Ingredient {
        public String name;
        public View view;
    }

    public void setHost(FragmentMenuTree host) {
        this.host = host;
    }

    public void initView(JSONObject object) {

        currentProduct = object;

        try {
            String name = object.has("name") ? object.getString("name") : "";

            JSONObject data = object.has("data") ? object.getJSONObject("data") : new JSONObject();
            String desc = data.has("desc") ? data.getString("desc") : "";
            String price = data.has("price") ? data.getString("price") : "";
            //String image = data.has("image") ? data.getString("image") : "http://media.cmgdigital.com/shared/img/photos/2012/03/31/f2/d8/big_mac_486218a.JPG";


            JSONArray ingredients = data.has("ingredients") ? data.getJSONArray("ingredients") : new JSONArray();


            et_name.setText(name);
            et_desc.setText(desc);
            et_price.setText(price);
            JSONArray photos = object.has("photos") ? object.getJSONArray("photos") : new JSONArray();

            if (photos.length() > 0) {
                JSONObject element = photos.getJSONObject(0);
                String path = element.has("path") ? element.getString("path") : "";

                SmartPitImageLoader.setImage(this.getActivity(), ivImage,
                        DAO.API_STATIC + path
                        , 0, 0);
            } else {
                ivImage.getImageView().setImageDrawable(this.getActivity().getResources().getDrawable(R.drawable.mock_product));
                ivImage.getProgressBar().setVisibility(View.GONE);
                ivImage.getImageView().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            }
            this.ingredients.removeAllViews();
            for (int i = 0; i < ingredients.length(); i++) {
                String ing = ingredients.getString(i);
                addIngredient(ing);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void clearView() {
        //  adapter.setActive(null);
        //  adapter.notifyDataSetChanged();
        et_name.setText("");
        et_desc.setText("");
        et_price.setText("");
    }


    private void addIngredient(String data) {

        final View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.row_ingredient, null);

        ImageView btn_remove = (ImageView) v.findViewById(R.id.btn_remove);

        TextView tv_label = (TextView) v.findViewById(R.id.tv_label);

        tv_label.setText(data);
        final Ingredient i = new Ingredient();
        i.name = data;
        i.view = v;

        btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ingredients.removeView(i.view);
                ingredientsList.remove(i);
            }
        });


        ingredientsList.add(i);
        ingredients.addView(i.view);
    }

    private JSONObject buidRequestData() {
        String name = et_name.getText().toString();
        double price = Double.parseDouble(et_price.getText().toString());

        String desc = et_desc.getText().toString();

        JSONObject data = new JSONObject();


        String category_id = null;
        try {
            category_id = parentCategory.has("_id") ? parentCategory.getString("_id") : "";
            data.put("category", category_id);


            data.put("owner", MyApplication.currentRestaurant.id);
            data.put("name", name);

            JSONObject details = new JSONObject();
            details.put("image", "http://www.sandiegomagazine.com/0001%20Jackson%20Images/everyday-eats-bfd-sandwich-1.jpg");

            details.put("price", price);
            details.put("desc", desc);

            JSONArray array = new JSONArray();
            for (int i = 0; i < ingredientsList.size(); i++) {
                array.put(ingredientsList.get(i).name);
            }

            details.put("ingredients", array);

            data.put("data", details);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }


    private void initProductForm(View v) {

        btn_add_ingredient = (TextView) v.findViewById(R.id.btn_add_ingredient);
        ingredients = (LinearLayout) v.findViewById(R.id.ingredients);
     //   et_ingredient = (EditText) v.findViewById(R.id.et_ingredient);
        tv_category = (TextView) v.findViewById(R.id.tv_category);

        et_name = (EditText) v.findViewById(R.id.et_name);
        et_price = (EditText) v.findViewById(R.id.et_price);
        et_desc = (EditText) v.findViewById(R.id.et_desc);

        etDescAnimation = new ShowingAnimation(this.getActivity(), et_desc);
        etNameAnimation = new ShowingAnimation(this.getActivity(), et_name);
        etPriceAnimation = new ShowingAnimation(this.getActivity(), et_price);


        ivImage = (SmartImageView) v.findViewById(R.id.add_photo);
        ivImage.setMode(SmartImageView.Mode.NORMAL.ordinal());
        ivImage.getImageView().setScaleType(ImageView.ScaleType.CENTER_CROP);

        btnComments = (SmartPitSelectorView) v.findViewById(R.id.btn_comments);
        btnGallery = (SmartPitSelectorView) v.findViewById(R.id.btn_gallery);

        btnGallery.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.white_gallery);
        btnGallery.getImageView().setPadding(padding, padding, padding, padding);

        btnComments.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.comment);
        btnComments.getImageView().setPadding(padding, padding, padding, padding);

        btnComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //        if (fm.findFragmentById(R.id.child_container) instanceof FragmentMenuProductComments)
                //            return;
                //        fm.beginTransaction().replace(R.id.child_container, new FragmentMenuProductComments()).commitAllowingStateLoss();
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentMenuProductGallery fragmentMenuProductGallery = new FragmentMenuProductGallery();
                fragmentMenuProductGallery.setArguments(FragmentMenuProduct.this.getArguments());
                host.refreashProductChildPanel(fragmentMenuProductGallery);


                //     if (fm.findFragmentById(R.id.child_container) instanceof FragmentMenuProductGallery)
                //         return;
                ///     fm.beginTransaction().replace(R.id.child_container, fragmentMenuProductGallery).commitAllowingStateLoss();

            }
        });


        btn_confirm = (SmartPitSelectorView) v.findViewById(R.id.btn_confirm);
        btn_confirm.configure(R.drawable.circle_green_light, R.drawable.circle_green, R.drawable.save);
        btn_confirm.getImageView().setPadding(paddingBig, paddingBig, paddingBig, paddingBig);

        btn_add_ingredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            //    String entry = et_ingredient.getText().toString();

             //   if (entry.equals(""))
             //       return;

          //      addIngredient(entry);
            }
        });

      //  btn_add_ingredient.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.plus);
      //  btn_add_ingredient.getImageView().setPadding(padding, padding, padding, padding);

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    JSONObject data = buidRequestData();

                    String id = null;
                    if (productData != null) {
                        id = productData.has("_id") ? productData.getString("_id") : null;
                    }
                    DAO.postProduct(new Response.Listener() {
                        @Override
                        public void onResponse(Object o) {
                            Log.d(TAG, o.toString());

                            //      if (FragmentMenuProduct.this.data == null)
                            //          initRootProducts();
                            //      else
                            //         initChildProducts();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            AppHelper.getErrorMessage(volleyError);
                            Log.d(TAG, volleyError.toString());
                        }
                    }, data, id);

                } catch (Throwable e) {
                    e.printStackTrace();
                }

            }
        });


    }


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menu_product, null);
        ingredientsList = new ArrayList<>();


        padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_small_padding);
        paddingBig = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);



        /*
        btnEdit = (SmartPitSelectorView) v.findViewById(R.id.btn_edit);
        btnEdit.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.pen);
        btnEdit.getImageView().setPadding(padding, padding, padding, padding);


        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleEdit();
            }
        });
        */
        initProductForm(v);
        if (this.getArguments() != null) {
            try {
                Log.d(TAG, "arguments " + this.getArguments().toString());

                if (this.getArguments().containsKey("parent"))
                    parentCategory = new JSONObject(this.getArguments().getString("parent"));
                if (this.getArguments().containsKey("data")) {

                    productData = new JSONObject(this.getArguments().getString("data"));
                    initView(productData);

                }


                Log.d(TAG, "arguments2 " + this.getArguments().toString());


            } catch (Throwable e) {
                e.printStackTrace();
            }
        }


        //toggleEdit();
        return v;
    }

    private void toggleEdit() {
        etDescAnimation.startAnimation();
        etNameAnimation.startAnimation();
        etPriceAnimation.startAnimation();

    }


    @Override
    public String getLabel() {
        return null;
    }
}
