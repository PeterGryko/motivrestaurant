package pl.slapps.motiv.motivrestaurant.placemenu;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SlidingPaneLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.widget.CustomSlidingPane;

/**
 * Created by piotr on 31.08.15.
 */
public class FragmentMenuTree extends SmartPitFragment {

    private CustomSlidingPane paneOne;
    private CustomSlidingPane paneTwo;
    private CustomSlidingPane paneThree;
    private CustomSlidingPane paneFour;
    private LinearLayout contentOne;
    private LinearLayout contentTwo;
    private LinearLayout contentThree;

    private String TAG = FragmentMenuTree.class.getName();

    private FragmentManager fm;

    private String label = "";


    public void refreashSecondPane(Bundle arg) {


        FragmentMenuCategories fc = new FragmentMenuCategories();
        fc.setHost(this);
        fc.setArguments(arg);

        FragmentMenuProduct fp = new FragmentMenuProduct();
        fp.setHost(this);
        //  fp.setArguments(this.getArguments());

        FragmentMenuProductsList fmpl = new FragmentMenuProductsList();
        fmpl.setHost(this);
        fmpl.setArguments(arg);

        fm.beginTransaction().replace(R.id.content_two, fc).replace(R.id.content_four, fp).replace(R.id.content_three, fmpl).commitAllowingStateLoss();


    }


    public void refreashProductPanel(JSONObject data) {
        closePane();

        FragmentMenuProduct fmp = new FragmentMenuProduct();

        fmp.setHost(this);
        Bundle arg = new Bundle();
        if (this.getArguments() != null)
            arg = this.getArguments();


        arg.putString("data", data.toString());
        fmp.setArguments(arg);
        FragmentMenuProductGallery fg = new FragmentMenuProductGallery();
        fg.setArguments(arg);

        fm.beginTransaction().replace(R.id.content_four, fmp).replace(R.id.content_five, fg).commitAllowingStateLoss();

        if (paneThree != null)
            paneThree.closePane();
    }

    public void initAddProductPanel() {
        closePane();

        FragmentMenuProduct fmp = new FragmentMenuProduct();

        fmp.setHost(this);
        Bundle arg = new Bundle();
        if (this.getArguments() != null)
            arg = this.getArguments();


        fmp.setArguments(arg);
        FragmentMenuProductGallery fg = new FragmentMenuProductGallery();
        fg.setArguments(arg);

        fm.beginTransaction().replace(R.id.content_four, fmp).replace(R.id.content_five, fg).commitAllowingStateLoss();
        if (paneThree != null)
            paneThree.closePane();
    }

    public void refreashProductChildPanel(SmartPitFragment fragment) {
        closeChildPane();
        fm.beginTransaction().replace(R.id.content_five, fragment).commitAllowingStateLoss();
        if(paneFour!=null)
            paneFour.closePane();
    }

    public boolean onBackPressed() {
        if (((MainActivity) FragmentMenuTree.this.getActivity()).getDrawerLayout().isDrawerOpen(((MainActivity) FragmentMenuTree.this.getActivity()).getDrawerContent())) {
            ((MainActivity) FragmentMenuTree.this.getActivity()).showMenu();
            return true;
        } else if (paneFour != null && !paneFour.isOpen()) {
            paneFour.openPane();
            return true;
        } else if (paneThree != null && !paneThree.isOpen()) {
            paneThree.openPane();
            return true;
        } else if (!paneTwo.isOpen()) {
            openChildPane();
            return true;
        } else if (!paneOne.isOpen()) {
            openPane();
            return true;
        }
        return super.onBackPressed();
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menu_tree, parent, false);

        paneOne = (CustomSlidingPane) v.findViewById(R.id.pane_one);
        paneTwo = (CustomSlidingPane) v.findViewById(R.id.pane_two);
        paneThree = (CustomSlidingPane) v.findViewById(R.id.pane_three);
        paneFour = (CustomSlidingPane) v.findViewById(R.id.pane_four);


        contentOne = (LinearLayout) v.findViewById(R.id.content_one);
        contentTwo = (LinearLayout) v.findViewById(R.id.content_two);
        contentThree = (LinearLayout) v.findViewById(R.id.content_three);

        if (this.getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

            contentOne.getLayoutParams().width = SmartPitAppHelper.getScreenWidth(this.getActivity()) / 3;
            contentTwo.getLayoutParams().width = SmartPitAppHelper.getScreenWidth(this.getActivity()) / 3;
            //contentThree.getLayoutParams().height=SmartPitAppHelper.getScreenHeight(this.getActivity());
        }
        fm = this.getChildFragmentManager();


        if (this.getArguments() == null) {


            FragmentMenuSlider fs = new FragmentMenuSlider();
            fs.setHost(this);
            FragmentMenuCategories fc = new FragmentMenuCategories();
            fc.setHost(this);

            FragmentMenuProduct fp = new FragmentMenuProduct();
            fp.setHost(this);


            FragmentMenuProductsList fmpl = new FragmentMenuProductsList();
            fmpl.setHost(this);


            fm.beginTransaction().replace(R.id.content_one, fs).replace(R.id.content_two, fc).replace(R.id.content_four, fp).replace(R.id.content_three, fmpl).commitAllowingStateLoss();


        } else {
            Log.d(TAG, "arguments " + this.getArguments().toString());


            JSONObject data = null;
            try {
                data = new JSONObject(this.getArguments().getString("parent"));

                label = data.has("label") ? data.getString("label") : "";
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setActionbarLabel();

            FragmentMenuSlider fs = new FragmentMenuSlider();
            fs.setHost(this);
            fs.setArguments(this.getArguments());

            FragmentMenuCategories fc = new FragmentMenuCategories();
            fc.setHost(this);
            fc.setArguments(this.getArguments());

            FragmentMenuProduct fp = new FragmentMenuProduct();
            fp.setHost(this);
            fp.setArguments(this.getArguments());

            FragmentMenuProductsList fmpl = new FragmentMenuProductsList();
            fmpl.setHost(this);
            fmpl.setArguments(this.getArguments());

            fm.beginTransaction().replace(R.id.content_one, fs).replace(R.id.content_two, fc).replace(R.id.content_four, fp).replace(R.id.content_three, fmpl).commitAllowingStateLoss();
        }


        paneOne.setPanelSlideListener(new SlidingPaneLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelOpened(View panel) {
                //initFragment();
            }

            @Override
            public void onPanelClosed(View panel) {
                Log.d(TAG, "on panel closed ");


                // ((MainActivity) FragmentMenuTree.this.getActivity()).switchFragmentWithoutAnimation(new FragmentMenuTree(), false);
            }
        });

        //openPane();

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                openPane();
                openChildPane();
            }
        });

        //fm.beginTransaction().add(R.id.content_three,new FragmentMenuProducts()).commitAllowingStateLoss();

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                paneOne.openPane();
                paneTwo.openPane();


                if (paneThree != null) {
                    paneThree.openPane();
                }
                if (paneFour != null) {
                    paneFour.openPane();
                }
            }
        });





        return v;
    }


    public void initFragment() {
        ((MainActivity) FragmentMenuTree.this.getActivity()).switchFragmentWithoutAnimation(new FragmentMenuTree(), false);

    }

    public void closePane() {
        if (paneOne.isOpen())
            paneOne.closePane();
    }

    public void openPane() {
        if (!paneOne.isOpen())
            paneOne.openPane();
    }

    public void openChildPane() {
        if (!paneTwo.isOpen())
            paneTwo.openPane();
    }

    public void closeChildPane() {
        if (paneTwo.isOpen())
            paneTwo.closePane();
    }


    @Override
    public String getLabel() {
        return label;
    }
}
