package pl.slapps.motiv.motivrestaurant.orders;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 31.08.15.
 */
public class AdapterProductsGrid extends BaseAdapter {

    private Drawable mock;
    private Context context;
    private ArrayList<JSONObject> data;

    public AdapterProductsGrid(Context context, ArrayList<JSONObject> data) {
        this.context = context;
        this.data = data;
        mock = context.getResources().getDrawable(R.drawable.black_food);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    class ViewHolder {
        public TextView tvLabel;
        public TextView tvPrice;
        public SmartImageView imageView;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        ViewHolder holder = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.row_order_grid, null);
            holder = new ViewHolder();
            holder.tvLabel = (TextView) view.findViewById(R.id.tv_label);
            holder.tvPrice = (TextView) view.findViewById(R.id.tv_price);
            holder.imageView = (SmartImageView) view.findViewById(R.id.image);
            holder.imageView.setMode(SmartImageView.Mode.NORMAL.ordinal());
            holder.imageView.getImageView().setScaleType(ImageView.ScaleType.CENTER_CROP);
            view.setTag(holder);

        } else
            holder = (ViewHolder) view.getTag();

        try {

            if (i < data.size()) {
                JSONObject element = data.get(i);
                String name = element.has("name") ? element.getString("name") : "";
                holder.tvLabel.setText(name);

                JSONObject data = element.has("data") ? element.getJSONObject("data") : new JSONObject();
                double price = data.has("price") ? data.getDouble("price") : 0;

                holder.tvPrice.setText(price + "zł");


                JSONArray photos = element.has("photos") ? element.getJSONArray("photos") : new JSONArray();

                if (photos.length() > 0) {
                    JSONObject photo = photos.getJSONObject(0);
                    String path = photo.has("path") ? photo.getString("path") : "";

                    if (!path.trim().equals(""))
                        SmartPitImageLoader.setImage(context, holder.imageView, DAO.API_STATIC + path, 0, 0);

                } else {
                    holder.imageView.getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.mock_product));
                    holder.imageView.getProgressBar().setVisibility(View.GONE);
                    holder.imageView.getImageView().setScaleType(ImageView.ScaleType.CENTER_INSIDE);

                }


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return view;
    }
}
