package pl.slapps.motiv.motivrestaurant.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.Response;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.adapter.AdapterFacebookSynchronization;
import pl.slapps.motiv.motivrestaurant.facebook.FacebookDataFetecher;
import pl.slapps.motiv.motivrestaurant.facebook.FacebookSynchronizer;

/**
 * Created by piotr on 19.04.15.
 */
public class FragmentFacebookSynchronization extends SmartPitFragment {

    private static final String TAG = FragmentFacebookSynchronization.class.getName();

    private ListView listView;
    private ArrayList<JSONObject> dataList;
    private Button btnLogin;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ((MainActivity) this.getActivity()).getCallbackManager().onActivityResult(requestCode, resultCode, data);
        //CallbackManager.
        Log.d(TAG, "on activity result fragment");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_facebook_synchronization, parent, false);
        listView = (ListView) v.findViewById(R.id.list_view);
        btnLogin = (Button) v.findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logInWithPublishPermissions(FragmentFacebookSynchronization.this, Arrays.asList("manage_pages"));

            }
        });
        dataList = new ArrayList<>();
        if (AccessToken.getCurrentAccessToken() != null) {



            new FacebookDataFetecher(FragmentFacebookSynchronization.this.getActivity(),new FacebookSynchronizer.SynchronizerListener() {
                @Override
                public void onElementLoaded(String key) {
                    Log.d(TAG,key);
                }

                @Override
                public void onLabelChange(String label) {

                }

                @Override
                public void onLoad(JSONObject object) {

                }
            }).wrapPagesResponse(new Response.Listener() {
                @Override
                public void onResponse(Object o) {
                    Log.d(TAG, o.toString());
                    try {
                        JSONObject object = new JSONObject(o.toString());


                        Iterator<String> keys = object.keys();
                        while (keys.hasNext()) {

                            String key = keys.next();

                            Log.d(TAG, key);

                            JSONObject data = object.getJSONObject(key);
                            dataList.add(data);

                        }


                        listView.setAdapter(new AdapterFacebookSynchronization((MainActivity) FragmentFacebookSynchronization.this.getActivity(), dataList));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


        }

        return v;
    }


    @Override
    public String getLabel() {
        return null;
    }


}