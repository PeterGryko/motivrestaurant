package pl.slapps.motiv.motivrestaurant.placemenu;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 16.09.15.
 */
public class ShowingAnimation {
    private View view;
    private Context context;

    public ShowingAnimation(Context context, View view) {
        this.context=context;
        this.view = view;
        initAnimations();
    }

    private Animation showInputAnimation = new Animation() {
        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            setShowingProgress(interpolatedTime);
        }
    };

    private Animation hideInputAnimation = new Animation() {
        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            setHidingProgress(interpolatedTime);
        }
    };

    private void setShowingProgress(float progress) {
        ViewCompat.setAlpha(view, progress);

    }

    private void setHidingProgress(float progress) {
        ViewCompat.setAlpha(view, 1 - progress);

    }


    private void initAnimations() {
        hideInputAnimation.setDuration(200);
        showInputAnimation.setDuration(200);

        hideInputAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                toggleEdit();
                view.clearAnimation();
                view.startAnimation(showInputAnimation);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void toggleEdit() {
        if (view.isEnabled()) {
            view.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
            view.setEnabled(false);
        } else {
            view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.abc_edit_text_material));
            view.setEnabled(true);
        }
    }

    public void startAnimation()
    {
        view.clearAnimation();
        view.startAnimation(hideInputAnimation);
    }
}

