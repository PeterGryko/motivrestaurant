package pl.slapps.motiv.motivrestaurant.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by piotr on 26.09.15.
 */
public class Period {
    public int day;
    public String open_time;
    public String close_time;


    public Period(JSONObject data) {
        try {
            JSONObject open = data.has("open") ? data.getJSONObject("open") : new JSONObject();
            JSONObject close = data.has("close") ? data.getJSONObject("close") : new JSONObject();

            day = open.has("day") ? open.getInt("day") : 0;

            open_time = open.has("time") ? open.getString("time") : "";
            close_time = close.has("time") ? close.getString("time") : "";

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}