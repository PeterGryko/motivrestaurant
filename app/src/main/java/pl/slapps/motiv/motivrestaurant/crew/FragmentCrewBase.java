package pl.slapps.motiv.motivrestaurant.crew;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;


import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.fragment.FragmentProfile;

/**
 * Created by piotr on 15.04.15.
 */
public class FragmentCrewBase extends SmartPitFragment {


    private MainActivity context;
    private ListView listView;
    private ProgressBar bar;


    private SlidingPaneLayout pane;

    private LinearLayout panelOne;
    private LinearLayout panelTwo;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_crew, null);


        pane = (SlidingPaneLayout) v.findViewById(R.id.pane);

        panelOne = (LinearLayout) v.findViewById(R.id.content_one);
        panelTwo = (LinearLayout) v.findViewById(R.id.content_two);

        if (this.getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

            int paneltWidth = (int) this.getActivity().getResources().getDimension(R.dimen.crew_left_panel_width);
            panelTwo.getLayoutParams().width = SmartPitAppHelper.getScreenWidth(this.getActivity()) - paneltWidth;
        }

        this.getChildFragmentManager().beginTransaction().add(R.id.content_one, new FragmentCrewList()).add(R.id.content_two, new FragmentCrewMember()).commitAllowingStateLoss();

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                pane.openPane();
            }
        });
        // pane.openPane();
        return v;
    }

    public void switchFragment(SmartPitFragment fragment) {
        this.getChildFragmentManager().beginTransaction().setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out).replace(R.id.content_two, fragment).addToBackStack(null).commitAllowingStateLoss();
    }

    @Override
    public String getLabel() {
        return null;
    }
}