package pl.slapps.motiv.motivrestaurant.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.crew.FragmentCrewBase;
import pl.slapps.motiv.motivrestaurant.myplace.FragmentEdit;
import pl.slapps.motiv.motivrestaurant.orders.FragmentNewOrderBase;
import pl.slapps.motiv.motivrestaurant.orders.FragmentOrdersBase;
import pl.slapps.motiv.motivrestaurant.placemenu.FragmentMenuTree;
import pl.slapps.motiv.motivrestaurant.splash.FragmentSplash;

/**
 * Created by piotr on 30.08.15.
 */
public class FragmentMainMenu extends SmartPitFragment {

    private String TAG = FragmentMainMenu.class.getName();
    private LinearLayout tabOrders;
    private LinearLayout tabNewOrder;
    private LinearLayout tabMyPlace;
    private LinearLayout tabMenu;
    private LinearLayout tabSettings;
    private LinearLayout tabCrew;

    private RelativeLayout baseNew;
    private RelativeLayout baseProduction;
    private RelativeLayout baseDeliver;

    //private TextView tvNew;
    //private TextView tvProduction;
    //private TextView tvDeliver;


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main_menu, parent, false);

        tabOrders = (LinearLayout) v.findViewById(R.id.tab_orders);
        tabNewOrder = (LinearLayout) v.findViewById(R.id.tab_new_order);
        tabMyPlace = (LinearLayout) v.findViewById(R.id.tab_my_place);
        tabMenu = (LinearLayout) v.findViewById(R.id.tab_menu);
        tabSettings = (LinearLayout) v.findViewById(R.id.tab_settings);
        tabCrew = (LinearLayout) v.findViewById(R.id.tab_crew);


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "on click");
            }
        };

        tabOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) FragmentMainMenu.this.getActivity()).switchFragment(new FragmentOrdersBase(), true);
            }
        });
        tabNewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) FragmentMainMenu.this.getActivity()).switchFragment(new FragmentNewOrderBase(), true);

            }
        });
        tabMyPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) FragmentMainMenu.this.getActivity()).switchFragment(new FragmentEdit(), true);
            }
        });
        tabMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) FragmentMainMenu.this.getActivity()).switchFragment(new FragmentMenuTree(), true);
            }
        });
        tabSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApplication.currentRestaurant = null;
                MyApplication.currentCrew = null;
                MyApplication.token = null;
                ((MainActivity) FragmentMainMenu.this.getActivity()).switchTitleFragment(new FragmentSplash(), true);

            }
        });
        tabCrew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) FragmentMainMenu.this.getActivity()).switchFragment(new FragmentCrewBase(), true);
            }
        });


        Log.d(TAG, MyApplication.getTokenUserRole());

        return v;
    }

    @Override
    public String getLabel() {
        if (MyApplication.currentRestaurant != null)
            return MyApplication.currentRestaurant.name;
        else
            return "";

    }
}
