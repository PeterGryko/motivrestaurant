package pl.slapps.motiv.motivrestaurant.orders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONException;
import org.json.JSONObject;

import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.model.Order;

/**
 * Created by piotr on 23.09.15.
 */
public class AdapterInfoWindow implements GoogleMap.InfoWindowAdapter {
    private Context context;

    public AdapterInfoWindow(Context context) {
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {

        String title = marker.getTitle().trim();

        View v = null;
        if (title.equals("restaurant")) {
            v = LayoutInflater.from(context).inflate(R.layout.info_window_place, null);
            TextView tvTitle = (TextView)v.findViewById(R.id.tv_name);
            tvTitle.setText(MyApplication.currentRestaurant.name);
        } else {
            v = LayoutInflater.from(context).inflate(R.layout.layout_info_window, null);
            try {
                Order o = new Order(new JSONObject(marker.getSnippet()));
                TextView tvClient = (TextView) v.findViewById(R.id.tv_client);
                TextView tvAddress = (TextView) v.findViewById(R.id.tv_address);
                TextView tvPrice = (TextView) v.findViewById(R.id.tv_price);

                tvClient.setText(o.title);
                tvAddress.setText(o.address);
                tvPrice.setText(o.price);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return v;
    }
}
