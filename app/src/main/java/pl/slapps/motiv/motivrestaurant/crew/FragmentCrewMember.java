package pl.slapps.motiv.motivrestaurant.crew;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.gryko.smartpitlib.widget.SmartPitRowedLayout;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.model.User;
import pl.slapps.motiv.motivrestaurant.widget.AppHelper;

/**
 * Created by piotr on 27.09.15.
 */
public class FragmentCrewMember extends SmartPitFragment {
    private SmartPitSelectorView btnEdit;
    private SmartPitSelectorView btnCall;
    private SmartPitSelectorView btnMsg;


    private int padding;

    private TextView tvUsername;
    private TextView tvPhone;
    private TextView tvStreet;
    private TextView tvFlat;
    private TextView tvHouse;
    private TextView tvCity;
    private TextView tvPostCode;

    private SmartImageView avatar;
    private SmartPitRowedLayout roles;


    private View getRoleLabelView(String data) {
        View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.role_layout, null);
        TextView label = (TextView) v.findViewById(R.id.tv_label);
        label.setText(data);
        return v;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crew_member, parent, false);

        padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);
        btnEdit = (SmartPitSelectorView) v.findViewById(R.id.btn_edit);
        btnEdit.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.pen);
        btnEdit.getImageView().setPadding(padding, padding, padding, padding);

        btnCall = (SmartPitSelectorView) v.findViewById(R.id.btn_call);
        btnCall.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.phone_white);
        btnCall.getImageView().setPadding(padding, padding, padding, padding);

        btnMsg = (SmartPitSelectorView) v.findViewById(R.id.btn_msg);
        btnMsg.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.send_message);
        btnMsg.getImageView().setPadding(padding, padding, padding, padding);

        tvCity = (TextView) v.findViewById(R.id.tv_city);
        tvPhone = (TextView) v.findViewById(R.id.tv_phone);
        tvPostCode = (TextView) v.findViewById(R.id.tv_postcode);
        tvStreet = (TextView) v.findViewById(R.id.tv_street);
        tvHouse = (TextView) v.findViewById(R.id.tv_house);
        tvFlat = (TextView) v.findViewById(R.id.tv_flat);
        tvUsername = (TextView) v.findViewById(R.id.tv_username);
        roles = (SmartPitRowedLayout) v.findViewById(R.id.base_roles);

        avatar = (SmartImageView) v.findViewById(R.id.avatar);

        avatar.getImageView().setImageDrawable(this.getResources().getDrawable(R.drawable.profile_cut));
        avatar.getProgressBar().setVisibility(View.GONE);

        if (this.getArguments() != null) {
            String arg = this.getArguments().getString("data");
            try {
                JSONObject data = new JSONObject(arg);
                User u = new User(data);
                tvCity.setText(u.locality);
                tvPhone.setText(u.phone);
                tvUsername.setText(u.name);
                tvPostCode.setText(u.postCode);
                tvStreet.setText(u.address);
                roles.addView(getRoleLabelView(u.role));


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((FragmentCrewBase) FragmentCrewMember.this.getParentFragment()).switchFragment(new FragmentAddCrewMember());

            }
        });

        btnMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.showMessageDialog(new JSONObject(), FragmentCrewMember.this.getActivity(), ((MainActivity) FragmentCrewMember.this.getActivity()).getService(), MyApplication.currentRestaurant.id);

            }
        });

        return v;
    }

    @Override
    public String getLabel() {
        return null;
    }
}
