package pl.slapps.motiv.motivrestaurant.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by piotr on 28.09.15.
 */
public class User {

    public String name;
    public String locality;
    public String phone;
    public String address;
    public String role;
    public String postCode;
    public JSONObject data;


    public User(JSONObject data) {
        this.data = data;

        try {
            this.role = data.has("role") ? data.getString("role") : "";

            data = data.has("data") ? data.getJSONObject("data") : new JSONObject();

            this.name = data.has("name") ? data.getString("name") : "";
            this.locality = data.has("locality") ? data.getString("locality") : "";
            this.phone = data.has("phone") ? data.getString("phone") : "";
            this.postCode = data.has("code") ? data.getString("code") : "";
            this.address = data.has("address") ? data.getString("address") : "";


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
