package pl.slapps.motiv.motivrestaurant.orders;

import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitMapFragment;
import pl.gryko.smartpitlib.route.SmartMapRouteManager;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.background.BackgroundService;
import pl.slapps.motiv.motivrestaurant.model.Driver;
import pl.slapps.motiv.motivrestaurant.model.Order;
import pl.slapps.motiv.motivrestaurant.widget.AppHelper;


/**
 * Created by piotr on 16.03.15.
 */
public class FragmentMap extends SmartPitMapFragment {


    String TAG = FragmentMap.class.getName();
    public static LatLng currentLocation;

    private boolean centered = false;

    private ArrayList<SmartMapRouteManager.Leg> currentRoute;
    private ArrayList<Marker> ordersMarkers;
    private ArrayList<Order> orders;
    private ArrayList<Marker> driversMarkers;

    private int map_camera_padding;
    private int map_bounds_width;
    private int map_bounds_height;

    private FragmentOrdersBase host;
    private AdapterInfoWindow adapter;
    private BackgroundService backgroundService;


    public void setHost(FragmentOrdersBase host) {
        this.host = host;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.smart_map_fragment, null);

        backgroundService = ((MainActivity) this.getActivity()).getService();
        ordersMarkers = new ArrayList<>();
        driversMarkers = new ArrayList<>();
        orders = new ArrayList<>();
        map_camera_padding = 30;
        //map_camera_padding = (int) this.getActivity().getResources().getDimension(R.dimen.map_camra_padding);
        map_bounds_width = (int) ((float) SmartPitAppHelper.getScreenWidth(this.getActivity()) / 1.5f);
        map_bounds_height = (int) ((float) SmartPitAppHelper.getScreenHeight(this.getActivity()) / 1.5f);
        adapter = new AdapterInfoWindow(this.getActivity());
        this.initMap();


        return v;
    }


    public void onMapInit(GoogleMap map) {

        this.getMap().setMyLocationEnabled(true);
        this.getMap().setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                Log.d(TAG, "on my location change listener");
                currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                if (!centered) {
                    FragmentMap.this.getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12));
                    centered = true;
                }
            }
        });
        this.getMap().setInfoWindowAdapter(adapter);
        this.getMap().setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {


                try {
                    host.initDetailView(new Order(new JSONObject(marker.getSnippet())));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //((MainActivity) FragmentMap.this.getActivity()).openPane();


            }
        });

        try {
            String photo = "http://www.clker.com/cliparts/v/P/R/e/p/3/hotel-icon-restaurant-clip-art-red-white-md.png";
            if (MyApplication.currentRestaurant.photos.size() > 0)
                photo = MyApplication.currentRestaurant.photos.get(0);
            new RemoteIconMapMarker().init(new LatLng(MyApplication.currentRestaurant.lat, MyApplication.currentRestaurant.lon),
                    photo
                    , "restaurant", MyApplication.currentRestaurant.name, this.getMap(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        refreashSelf();
    }

    public void refreashSelf() {
        if (backgroundService != null) {
            ArrayList<Order> orders = backgroundService.getOrders();
            refreashMarkers(orders);
        }
    }

    public void refreashMarkers(ArrayList<Order> data) {
        if (this.getMap() == null)
            return;


        if (host != null) {
            int currentPage = host.getCurrentTab();
            switch (currentPage) {
                case 0:
                    if (!MyApplication.getTokenUserRole().equals("driver")) {

                        data = AppHelper.filterOrders("new", data);
                    } else {
                        data = AppHelper.filterOrders("to_deliver", data);
                    }
                    break;
                case 1:
                    if (!MyApplication.getTokenUserRole().equals("driver")) {

                        data = AppHelper.filterOrders("production", data);
                    } else {
                        data = AppHelper.filterOrders("on_way", data);

                    }
                    break;
                case 2:
                    if (!MyApplication.getTokenUserRole().equals("driver")) {

                        data = AppHelper.filterOrders("to_deliver", data);
                    } else {
                        data = AppHelper.filterOrders("done", data);

                    }
                    break;
            }
        }

        orders = data;
        for (int i = 0; i < ordersMarkers.size(); i++) {
            ordersMarkers.get(i).remove();
        }


        for (int i = 0; i < data.size(); i++) {
            Order order = data.get(i);

            ordersMarkers.add(this.getMap().addMarker(new MarkerOptions().snippet(order.data.toString()).title(order.id).position(new LatLng(order.latitude, order.longitude))));

        }


        animateMap(data);


    }

    public void resetCameraBasedOnMarkers() {
        if (this.getMap() != null) {
            animateMap(orders);
        }
    }

    private void animateMap(ArrayList<LatLng> points, GoogleMap map) {
        if (points.size() > 0) {
            LatLngBounds.Builder b = new LatLngBounds.Builder();
            for (LatLng p : points) {
                b.include(p);
            }

            LatLngBounds bounds = b.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, map_bounds_width,
                    map_bounds_height,
                    map_camera_padding);
            this.getMap().animateCamera(cu);
        }
    }

    public void animateMap(ArrayList<Order> places) {
        if (places.size() > 0) {
            LatLngBounds.Builder b = new LatLngBounds.Builder();
            for (Order p : places) {
                b.include(new LatLng(p.latitude, p.longitude));
            }
            LatLngBounds bounds = b.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, map_bounds_width,
                    map_bounds_height,
                    map_camera_padding);
            this.getMap().animateCamera(cu);

//Change the padding as per needed

        }
    }

    public void toggleRoute(final Order order) {

        currentLocation = new LatLng(53.116379, 23.162304);
        if (currentLocation == null) {
            Toast.makeText(this.getActivity(), "Brak aktualnej pozycji", Toast.LENGTH_LONG).show();
            return;
        }
        if (currentRoute != null) {
            for (int i = 0; i < currentRoute.size(); i++) {
                ArrayList<SmartMapRouteManager.Step> steps = currentRoute.get(i).steps;
                for (int j = 0; j < steps.size(); j++) {
                    steps.get(j).polyline.remove();
                }
            }
            currentRoute = null;
            centerMarker(order);
            return;
        }


        new SmartMapRouteManager().showRoute(new LatLng(currentLocation.latitude, currentLocation.longitude),
                new LatLng(order.latitude, order.longitude),
                "AIzaSyCXuTyeq9UBftKWdILyb6-E4Q7_Kp1Y16Q"
                , new SmartMapRouteManager.RouteListener() {
                    @Override
                    public void success(ArrayList<SmartMapRouteManager.Leg> list) {

                        ArrayList<LatLng> points = new ArrayList<LatLng>();
                        currentRoute = list;
                        for (int i = 0; i < currentRoute.size(); i++) {
                            ArrayList<SmartMapRouteManager.Step> steps = currentRoute.get(i).steps;
                            for (int j = 0; j < steps.size(); j++) {

                                points.addAll(steps.get(j).drawLine(FragmentMap.this.getMap()).getPoints());

                            }
                        }

                        animateMap(points, FragmentMap.this.getMap());


                    }

                    @Override
                    public void failure() {
                        Log.d(TAG, "drawing roiute failure");

                    }
                });
    }


    public void refreashDrivers(ArrayList<Driver> data) {
        if (this.getMap() == null)
            return;

        for (int i = 0; i < driversMarkers.size(); i++) {
            driversMarkers.get(i).remove();
        }

        for (int i = 0; i < data.size(); i++) {
            Driver driver = data.get(i);
            driversMarkers.add(this.getMap().addMarker(new MarkerOptions().title(driver.username).position(new LatLng(driver.
                    latitude, driver.longitude)).
                    icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))));


        }
    }

    public void centerMarker(Order order) {
        if (this.getMap() == null)
            return;
        this.getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(order.latitude, order.longitude), 13));
    }


    public String getLabel() {
        return null;
    }
}
