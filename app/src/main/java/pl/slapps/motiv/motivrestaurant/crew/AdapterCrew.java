package pl.slapps.motiv.motivrestaurant.crew;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.model.User;

/**
 * Created by piotr on 06.09.15.
 */
public class AdapterCrew extends ArrayAdapter {

    private String TAG = AdapterCrew.class.getName();
    private Context context;
    private ArrayList<User> list;

    public AdapterCrew(Context context, ArrayList<User> list) {
        super(context, R.layout.row_orders_list, list);
        this.context = context;
        this.list = list;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.row_crew, null);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
        TextView tvRole = (TextView) convertView.findViewById(R.id.tv_role);

        SmartImageView logo = (SmartImageView) convertView.findViewById(R.id.iv_logo);

        logo.getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.profile_cut));
        logo.getProgressBar().setVisibility(View.GONE);


        tvName.setText(list
                .get(position).name);
        tvRole.setText(list.get(position).role);


        return convertView;

    }
}
