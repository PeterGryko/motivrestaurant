package pl.slapps.motiv.motivrestaurant.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;



import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 18.04.15.
 */
public class AdapterFacebookWall extends ArrayAdapter {
    private MainActivity context;
    private ArrayList<JSONObject> list;
    int lastPosition = 0;

    public AdapterFacebookWall(MainActivity context, ArrayList<JSONObject> list) {
        super(context, R.layout.row_facebook_wall, list);
        this.context = context;
        this.list = list;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(R.layout.row_facebook_wall, null);

        JSONObject element = list.get(position);
        String message = null;
        String picture = null;
        try {
            message = element.has("message") ? element.getString("message") : "";
            picture = element.has("picture") ? element.getString("picture") : "";



            SmartImageView image = (SmartImageView)convertView.findViewById(R.id.iv_pic);
            image.setMode(SmartImageView.Mode.NORMAL.ordinal());
            TextView tvMessage = (TextView) convertView.findViewById(R.id.tv_message);

            SmartPitImageLoader.setImage(context,image,picture,0,0);


            tvMessage.setText(message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return convertView;
    }


}
