package pl.slapps.motiv.motivrestaurant.splash;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.BufferedReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.motiv.motivrestaurant.MainActivity;


/**
 * Created by piotr on 05.04.15.
 */
public class AdapterPlaces extends ArrayAdapter<String> implements
        Filterable {

    public class PlaceModel {
        public String name;
        public String id;
    }

    private String TAG = "AdapterDeliveryAddress";
    private RequestQueue jr;
    private MainActivity context;
    private ArrayList<PlaceModel> list = new ArrayList<PlaceModel>();
    private ArrayList<PlaceModel> res = new ArrayList<PlaceModel>();

    private TextView tv_label;


    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            list.removeAll(list);
            list.addAll(res);

            if (tv_label != null) {
                if (list.size() > 0)
                    tv_label.setVisibility(View.GONE);
                else
                    tv_label.setVisibility(View.GONE);
            }
            AdapterPlaces.this.notifyDataSetChanged();
        }
    };

    public AdapterPlaces(MainActivity context, TextView tv_label) {
        super(context, android.R.layout.simple_list_item_1);
        // TODO Auto-generated constructor stub
        jr = Volley.newRequestQueue(context);
        this.context = context;
        this.tv_label = tv_label;

    }

    public int getCount() {
        return list.size();
    }

    public String getItem(int index) {
        return list.get(index).name;
    }

    public PlaceModel getPoint(int index) {
        return list.get(index);
    }


    @Override
    public Filter getFilter() {
        // TODO Auto-generated method stub
        return new Filter() {

            @Override
            protected FilterResults performFiltering(final CharSequence arg0) {
                // TODO Auto-generated method stub

                Log.d(TAG, "perform filtering " + arg0);
                if (arg0 == null) {
                    res = new ArrayList<>();
                    handler.sendMessage(handler.obtainMessage());
                } else {
                    new FilterAddresses(arg0.toString(), handler).start();
                }
                FilterResults result = new FilterResults();
                result.values = list;
                result.count = list.size();

                return result;
            }

            @Override
            protected void publishResults(CharSequence arg0, FilterResults arg1) {

            }
        };
    }


    class FilterAddresses extends Thread {


        class MixedResults {

            ArrayList<PlaceModel> places;
            ArrayList<PlaceModel> dataList;

            public MixedResults(ArrayList<PlaceModel> data) {
                this.dataList = data;
            }


            public void populatePlaces(AutocompletePredictionBuffer autocompletePredictions) {

                Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
                ArrayList<PlaceModel> tmp = new ArrayList<>();
                while (iterator.hasNext()) {
                    AutocompletePrediction prediction = iterator.next();


                    PlaceModel model = new PlaceModel();
                    model.id = prediction.getPlaceId();
                    model.name = prediction.getDescription();


                    tmp.add(model);


                }
                autocompletePredictions.release();
                places = tmp;

                sendResult();

            }


            private void sendResult() {


                for (int i = 0; i < places.size(); i++) {
                    populateResult(places.get(i), dataList);
                }


                Message msg = handler.obtainMessage();
                handler.sendMessage(msg);
            }

        }

        private String TAG = FilterAddresses.class.getName();

        private URL url;
        private HttpURLConnection httpConn;
        private BufferedReader br;
        ;
        private String arg0;
        private Handler handler;

        public FilterAddresses(String arg0, Handler handler) {
            this.arg0 = arg0;
            this.handler = handler;
        }


        @Override
        public void run() {


            try {

                if (res != null)
                    res.removeAll(res);
                else
                    res = new ArrayList<PlaceModel>();
                final MixedResults mr = new MixedResults(res);

                Log.d(TAG, "start motiv query " + arg0);


                PendingResult<AutocompletePredictionBuffer> results =
                        Places.GeoDataApi
                                .getAutocompletePredictions(context.mGoogleApiClient, arg0,
                                        new LatLngBounds(new LatLng(51.057159, 14.986014), new LatLng(54.177114, 22.808280)), null);

                // This method should have been called off the main UI thread. Block and wait for at most 60s
                // for a result from the API.
                final AutocompletePredictionBuffer autocompletePredictions = results
                        .await(5, TimeUnit.SECONDS);
                final Status status = autocompletePredictions.getStatus();
                if (!status.isSuccess()) {

                    Log.e(TAG, "Error getting autocomplete prediction API call: " + status.toString());
                    autocompletePredictions.release();

                }

                mr.populatePlaces(autocompletePredictions);


            } catch (Throwable e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    public void populateResult(PlaceModel element, ArrayList<PlaceModel> list) {
        list.add(element);

    }
}