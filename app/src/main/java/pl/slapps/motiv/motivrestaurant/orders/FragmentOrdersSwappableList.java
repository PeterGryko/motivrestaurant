package pl.slapps.motiv.motivrestaurant.orders;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;


import java.util.ArrayList;

import pl.gryko.smartpitlib.interfaces.SmartPitFragmentsInterface;
import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.background.BackgroundService;
import pl.slapps.motiv.motivrestaurant.draganddrop.DragNDropListView;
import pl.slapps.motiv.motivrestaurant.model.Order;

/**
 * Created by piotr on 07.05.15.
 */
public class FragmentOrdersSwappableList {

    private String TAG = FragmentOrdersSwappableList.class.getName();

    private DragNDropListView listView;
    private Context context;
    private SmartPitFragmentsInterface listener;
    private ArrayList<Order> data;
    private Button btnStart;
    private FragmentOrdersBase parent;


    // private ProgressBar bar;

    public View init(final FragmentOrdersBase parent, final SmartPitFragmentsInterface listener, final ArrayList<Order> data) {
        this.parent=parent;
        this.context = parent.getActivity();
        this.listener = listener;
        this.data = data;
        View v = LayoutInflater.from(context).inflate(R.layout.fragment_orders_in_progress, null);
        listView = (DragNDropListView) v.findViewById(R.id.lv);
        btnStart = (Button)v.findViewById(R.id.btn_start);
        listView.setDraggingEnabled(true);
        listView.setDragNDropAdapter(new AdapterOrdersSwappableList(context, data, listener, listView, R.id.grab_base));





        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FragmentOrdersSwappableList.this.context, BackgroundService.class);
                FragmentOrdersSwappableList.this.context.startService(i);
            }
        });

        //bar = (ProgressBar) v.findViewById(R.id.bar);

        //if (data.size() > 0)
        //    bar.setVisibility(View.GONE);

        refreashData(data);

        return v;
    }

    public void refreashData(final ArrayList<Order> data) {

        Log.d(TAG, "refresh data " + data.size());

        this.data = data;
        try {

          /*  ArrayList<String> entries = new ArrayList<String>();
            for (int i = 0; i < data.size(); i++) {

                entries.add(data.get(i).title);
            }
*/
            // if (data.size() > 0 && bar.getVisibility() == View.VISIBLE)
            //   bar.setVisibility(View.GONE);
      //      listView.setAdapter(new AdapterOrdersInProgress(context, data, listener, listView));



            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //FragmentOrdersSwappableList.this.parent.initDetailView(data.get(position));
                }
            });

        } catch (Throwable t) {
            Log.d(TAG, t.toString());
        }
    }


}
