package pl.slapps.motiv.motivrestaurant.adapter;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.facebook.AccessToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.facebook.FacebookDataFetecher;
import pl.slapps.motiv.motivrestaurant.facebook.FacebookSyncDialog;
import pl.slapps.motiv.motivrestaurant.facebook.FacebookSynchronizer;

/**
 * Created by piotr on 19.04.15.
 */
public class AdapterFacebookSynchronization extends ArrayAdapter {

    private String TAG = AdapterFacebookSynchronization.class.getName();

    private MainActivity context;
    private ArrayList<JSONObject> list;
    private FacebookSyncDialog syncDialog;

    public AdapterFacebookSynchronization(MainActivity context, ArrayList<JSONObject> list) {
        super(context, R.layout.row_facebook_app, list);

        this.list = list;
        this.context = context;
        syncDialog = new FacebookSyncDialog(context);

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.row_facebook_app, null);
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
        SmartImageView bkg = (SmartImageView) convertView.findViewById(R.id.iv_bkg);
        bkg.setMode(SmartImageView.Mode.NORMAL.ordinal());
        bkg.getImageView().setScaleType(ImageView.ScaleType.CENTER_CROP);

        ImageView ivFacebook = (ImageView) convertView.findViewById(R.id.iv_facebook);
        ImageView ivSynch = (ImageView) convertView.findViewById(R.id.iv_sync);


        JSONObject element = list.get(position);


        try {
            JSONObject cover = element.has("cover") ? element.getJSONObject("cover") : new JSONObject();
            String url = cover.has("source") ? cover.getString("source") : "";

            String name = element.has("name") ? element.getString("name") : "";
            final String link = element.has("link") ? element.getString("link") : "";
            final String id = element.has("id") ? element.getString("id") : "";

            Log.d(TAG, link + " " + id);

            tvName.setText(name);

            SmartPitImageLoader.setImage(context, bkg, url, 0, 0);

            ivSynch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    syncDialog.showDialog();
                    new FacebookDataFetecher(context, new FacebookSynchronizer.SynchronizerListener() {
                        @Override
                        public void onElementLoaded(String key) {
                            //   Log.d(TAG, "on progress " + key);
                        }

                        @Override
                        public void onLabelChange(String label) {

                            syncDialog.setLabel(label);

                            Log.d(TAG, "label changed " + label);
                        }

                        @Override
                        public void onLoad(JSONObject object) {
                            Log.d(TAG, object.toString());

                            syncDialog.dismissDialog();
                        }
                    }).buildProfileData(id, AccessToken.getCurrentAccessToken().getToken());
                }
            });

            ivFacebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = null;
                    try {
                        context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
                        i = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + id));
                    } catch (Exception e) {
                        i = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                    }
                    context.startActivity(i);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return convertView;
    }
}
