package pl.slapps.motiv.motivrestaurant.DAO;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import pl.gryko.smartpitlib.SmartPitActivity;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartMultipartRequest;
import pl.slapps.motiv.motivrestaurant.MyApplication;

/**
 * Created by piotr on 16.03.15.
 */
public class DAO {
    private static String TAG = DAO.class.getName();
    private static int MY_TIMEOUT = 100 * 1000;

    public static final String API_URL = "http://188.166.101.11:";
    public static final int API_PORT = 7990;
   // public static final String API = API_URL + API_PORT + "/";

    public static final String API_NEW = "http://188.166.101.11:7990/v1/";
    public static final String API_STATIC = "http://188.166.101.11:7990/";

    public static void getProfile(Response.Listener listener, Response.ErrorListener errorListener) {
        String request = API_NEW + "profile/";


        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener) {
            public HashMap<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", MyApplication.token);
                return headers;
            }
        };
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }


    public static void postProduct(Response.Listener listener, Response.ErrorListener errorListener, final JSONObject data, String id) {
        String request = "";
        if (id == null)
            request = API_NEW + "product";
        else
            request = API_NEW + "product?_id=" + id;

        Log.d(TAG, request);


        StringRequest jr = new StringRequest(Request.Method.POST, request,
                listener, errorListener) {
            public byte[] getBody() {


                return data.toString().getBytes();
            }

            public HashMap<String, String> getHeaders() {
                HashMap<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json ");
                map.put("token", MyApplication.token);

                return map;
            }
        };
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }

    public static void getProducts(Response.Listener listener, Response.ErrorListener errorListener, String restaurant_id, String category_id) {

        String request = "";
        if (category_id == null)
            request = API_NEW + "product?find.owner=" + restaurant_id + "&method=getRoots";
        else
            request = API_NEW + "product?find.category=" + category_id;


        Log.d(TAG, request);


        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }

    public static void getProduct(Response.Listener listener, Response.ErrorListener errorListener, String id) {

        String request = "";

        request = API_NEW + "product?_id=" + id;


        request += "&populate.photos";

        Log.d(TAG, request);


        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }


    public static void setRestaurantNode(Response.Listener listener, Response.ErrorListener errorListener, final String id, String nodeName, final JSONObject node) {
        String request = API_NEW + "restaurant/" + id + "/set/" + nodeName;

        Log.d(TAG, request);
        Log.d(TAG, node.toString());


        StringRequest jr = new StringRequest(Request.Method.POST, request,
                listener, errorListener) {
            public byte[] getBody() {


                return node.toString().getBytes();
            }

            public HashMap<String, String> getHeaders() {
                HashMap<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json ");
                return map;
            }
        };
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }


    public static void getCategories(String id, Response.Listener listener, Response.ErrorListener errorListener, String parentId) {
        String request = "";
        if (parentId == null)
            request = API_NEW + "category?find.owner=" + id + "&method=getRoots";
        else
            request = API_NEW + "category?_id=" + parentId + "&method=getChildren";

        Log.d(TAG, request);


        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }


    public static void addCategory(Response.Listener listener, Response.ErrorListener errorListener, String restaurantId, String parentId, String category) {
        String request = API_NEW + "category";

        Log.d(TAG, request);

        final JSONObject data = new JSONObject();


        try {
            data.put("owner", restaurantId);
            data.put("label", category);
            if (parentId != null)
                data.put("parent", parentId);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest jr = new StringRequest(Request.Method.PUT, request,
                listener, errorListener) {
            public byte[] getBody() {


                return data.toString().getBytes();
            }

            public HashMap<String, String> getHeaders() {
                HashMap<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json ");
                return map;
            }
        };
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }


    public static void getMyPlaces(
            Response.Listener successListener,
            Response.ErrorListener errorListener) {


        //String request = API + "api-token-auth/";

        String request = API_NEW + "restaurant?my&populate.place=name%20formatted_address%20google&populate._crew=data%20role&token=" + MyApplication.token;

        Log.d(TAG, request);


        StringRequest jr = new StringRequest(Request.Method.GET, request,
                successListener, errorListener) {


            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("token", MyApplication.token);
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };


        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }

    public static void createPlace(
            Response.Listener successListener,
            Response.ErrorListener errorListener, final JSONObject data) {


        //String request = API + "api-token-auth/";


        String request = API_NEW + "restaurant";

        Log.d(TAG, request);


        StringRequest jr = new StringRequest(Request.Method.POST, request,
                successListener, errorListener) {


            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("token", MyApplication.token);
                headers.put("Content-Type", "application/json");
                return headers;
            }

            public byte[] getBody() {
                return data.toString().getBytes();
            }

        };


        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }



    public static void getRestaurantData(Response.Listener listener, Response.ErrorListener errorListener, String id) {
        String request = API_NEW + "place?_id=" + id;

        Log.d(TAG, request);
        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }


    /*
    public static void uploadRestaurantLogo(Response.Listener listener, Response.ErrorListener errorListener, File file) {
        String request = API + "restaurant/5518b44cc69a759d4a8dad6d/set/logo";


        SmartMultipartRequest jr = new SmartMultipartRequest(request,
                errorListener, listener, file, "");
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }
*/
    public static void loginUser(final JSONObject data,
                                 Response.Listener successListener,
                                 Response.ErrorListener errorListener) {


        //String request = API + "api-token-auth/";

        String request = API_NEW + "User/auth";

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.POST, request,
                successListener, errorListener) {
            @Override
            public byte[] getBody() {
                return data.toString().getBytes();
            }

            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };


        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }

    /*
        public static void uploadRestaurantMedia(Response.Listener listener, Response.ErrorListener errorListener, File file) {
            String request = API + "restaurant/5518b44cc69a759d4a8dad6d/push/media";


            SmartMultipartRequest jr = new SmartMultipartRequest(request,
                    errorListener, listener, file, "");
            jr.setRetryPolicy(new DefaultRetryPolicy(
                    MY_TIMEOUT,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue rq = SmartPitActivity.getRequestQueue();

            rq.add(jr);
            rq.start();
        }
    */
    public static void getAllOrders(Response.Listener listener, Response.ErrorListener errorListener, String id) {
        String request = API_NEW + "order?find.owner=" + id;
        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }

    public static void getDrivers(Response.Listener listener, Response.ErrorListener errorListener) {
        String request = "http://private-a71c9-restaurantmotiv.apiary-mock.com/drivers";


        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }

    public static void getRoad(Response.Listener listener, Response.ErrorListener errorListener, String request) {
        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }

    public static void getFacebookWall(Response.Listener listener, Response.ErrorListener errorListener) {

        String request = "https://graph.facebook.com/636032173146683/feed?access_token=CAACEdEose0cBADKEhpU4c6b5De6hfyTQbCowFx6SaS13U2bS2zKu2qdZBdtVY4NuN7uIGG8AKsZBymGLZBfc7Y1mnYrLJxBNFU9n0V0jjnnZCAR88ZA2YQl13xGL2jwZCKu5EOeIKf55xrhZB2r0BkUvlVRnqB08eh4jZBL6XiB8tmXr02KUPyrLiZAtq9aBZBrs1q4H1X46ZCo3oTZCCGmNFZB4xh3j8l6zLr10ZD";
        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }

    public static void getImageDetails(Response.Listener listener, Response.ErrorListener errorListener) {

        String request = "https://graph.facebook.com/636032173146683/feed?access_token=CAACEdEose0cBADKEhpU4c6b5De6hfyTQbCowFx6SaS13U2bS2zKu2qdZBdtVY4NuN7uIGG8AKsZBymGLZBfc7Y1mnYrLJxBNFU9n0V0jjnnZCAR88ZA2YQl13xGL2jwZCKu5EOeIKf55xrhZB2r0BkUvlVRnqB08eh4jZBL6XiB8tmXr02KUPyrLiZAtq9aBZBrs1q4H1X46ZCo3oTZCCGmNFZB4xh3j8l6zLr10ZD";
        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.GET, request,
                listener, errorListener);
        jr.setRetryPolicy(new DefaultRetryPolicy(
                MY_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();
    }

    public static void updateFile(final File file,
                                  Response.Listener successListener,
                                  Response.ErrorListener errorListener) {


        String request = API_NEW + "files";

        Log.d(TAG, request);

        SmartMultipartRequest jr = new SmartMultipartRequest(Request.Method.PUT, request,
                errorListener, successListener, file, "file", null) {
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("token", MyApplication.token);
                return headers;
            }
        };


        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }

    public static void createOrder(final JSONObject data,
                                   Response.Listener successListener,
                                   Response.ErrorListener errorListener) {


        String request = API_NEW + "order";

        Log.d(TAG, request);

        StringRequest jr = new StringRequest(Request.Method.POST, request,
                successListener, errorListener) {
            @Override
            public byte[] getBody() {
                return data.toString().getBytes();
            }

            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("token", MyApplication.token);
                return headers;
            }
        };


        RequestQueue rq = SmartPitActivity.getRequestQueue();

        rq.add(jr);
        rq.start();

    }


}



