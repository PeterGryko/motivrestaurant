package pl.slapps.motiv.motivrestaurant.facebook;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.facebook.AccessToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MyApplication;

/**
 * Created by piotr on 19.04.15.
 */
public class FacebookDataFetecher {

    private String FETCHING_PROFILE = "Pobieranie danych użytkownika";
    private String FETCHING_PROFILE_IMAGE = "Pobieranie zdjęcia profilowego";
    private String FETCHING_GALLERY = "Pobieranie galerii";

    private String TAG = FacebookDataFetecher.class.getName();
    private Context context;
    private FacebookSynchronizer synchronizer;
    private HashMap<String, Object> tasksList;
    private FacebookSynchronizer.SynchronizerListener synchronizerListener;

    public FacebookDataFetecher(Context context, FacebookSynchronizer.SynchronizerListener synchronizerListener) {
        this.context = context;
        this.synchronizerListener=synchronizerListener;
        this.synchronizer = new FacebookSynchronizer(context,synchronizerListener);
        tasksList = new HashMap<>();
    }


    public void buildProfileData(final String id, final String token) {



        final Object profileDetailsTask = new Object();
       // synchronizer.addTask(profileDetailsTask);
        synchronizer.getSynchListener().onLabelChange(FETCHING_PROFILE);
        Http.getFacebookProfileDetails(new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());

                try {
                    final JSONObject returnData = new JSONObject();


                    JSONObject object = new JSONObject(o.toString());
                    JSONObject facebookLocation = object.has("location") ? object.getJSONObject("location") : new JSONObject();
                    String city = facebookLocation.has("city") ? facebookLocation.getString("city") : "";
                    String country = facebookLocation.has("country") ? facebookLocation.getString("country") : "";
                    double latitude = facebookLocation.has("latitude") ? facebookLocation.getDouble("latitude") : 0;
                    double longitude = facebookLocation.has("longitude") ? facebookLocation.getDouble("longitude") : 0;
                    String street = facebookLocation.has("street") ? facebookLocation.getString("street") : "";
                    String zip = facebookLocation.has("zip") ? facebookLocation.getString("zip") : "";

                    JSONObject coverObj = object.has("cover")?object.getJSONObject("cover"):new JSONObject();
                    String source = coverObj.has("source")?coverObj.getString("source"):"";





                    final JSONObject geojson = new JSONObject();
                    geojson.put("city", city);
                    geojson.put("country", country);
                    geojson.put("street", street);
                    geojson.put("lat", latitude);
                    geojson.put("lon", longitude);

                    returnData.put("geojson", geojson);

                    String name = object.has("name") ? object.getString("name") : "";
                    String desc = object.has("about") ? object.getString("about") : "";

                    JSONObject personalData = new JSONObject();
                    personalData.put("name", name);
                    personalData.put("desc", desc);
                    personalData.put("cover",source);

                    returnData.put("data", personalData);

                    Log.d(TAG,"personal data "+personalData.toString());


                    //final boolean dataSynched = SmartPitAppHelper.getInstance(context).getPreferences().getBoolean("synchronized", false);


                    getProfilePicture(returnData, id, token);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                synchronizer.removeTask(profileDetailsTask);
                // tasksList.remove(profileDetailsTask)
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }, id, token);

    }

    private void getUploadedImages(final JSONObject returnData, String id, String token) {

        synchronizer.getSynchListener().onLabelChange(FETCHING_GALLERY);

        final Object uploadedTask = new Object();
       // synchronizer.addTask(uploadedTask);
        Http.getUploadedImages(new Response.Listener() {
            @Override
            public void onResponse(Object o) {

                Log.d(TAG,"media "+o.toString());

                try {
                    JSONObject inputData = new JSONObject(o.toString());



                    JSONArray arrayData = inputData.has("data") ? inputData.getJSONArray("data") : new JSONArray();
                    final JSONArray media = new JSONArray();
                    for (int i = 0; i < arrayData.length(); i++) {

                        JSONObject facebookElement = arrayData.getJSONObject(i);
                        String path = facebookElement.has("source") ? facebookElement.getString("source") : "";
                        //Log.d(TAG,path);
                        JSONObject mediaElement = new JSONObject();
                        mediaElement.put("path", path);
                        media.put(mediaElement);

                    }

                    Log.d(TAG,"media size "+media.length());

                    returnData.put("media", media);

                    synchronizer.synchronizeData(returnData);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                synchronizer.removeTask(uploadedTask);



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }, id, token);

    }


    public void getProfilePicture(final JSONObject returnData, final String id, final String token) {
        final Object profileTask = new Object();
       // synchronizer.addTask(profileTask);
        synchronizer.getSynchListener().onLabelChange(FETCHING_PROFILE_IMAGE);

        Http.getProfileImage(new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                JSONObject object = null;
                try {
                    object = new JSONObject(o.toString());

                    JSONObject data = object.has("data") ? object.getJSONObject("data") : new JSONObject();
                    String url = data.has("url") ? data.getString("url") : "";
                    JSONObject logo = new JSONObject();
                    logo.put("path", url);
                    returnData.put("logo", logo);

                    //synchronizer.synchronizeData(returnData);

                    getUploadedImages(returnData, id, token);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                synchronizer.removeTask(profileTask);
                //tasksList.remove(profileTask);
                //loaderListener.onLoad(returnData);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }, id, token);
    }


    public void wrapPagesResponse(final Response.Listener listener) {

        final ArrayList<String> dataList = new ArrayList<>();
        Http.getProfilePages(new Response.Listener() {
            @Override
            public void onResponse(Object o) {
                Log.d(TAG, o.toString());
                try {
                    JSONObject object = new JSONObject(o.toString());
                    JSONArray dataArray = object.has("data") ? object.getJSONArray("data") : new JSONArray();

                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject element = dataArray.getJSONObject(i);
                        String id = element.has("id") ? element.getString("id") : "";
                        dataList.add(id);

                    }

                    getDetails(dataList, listener);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, volleyError.toString());

            }
        }, "", AccessToken.getCurrentAccessToken().getToken());

    }

    private void getDetails(ArrayList<String> dataList, Response.Listener listener) {

        Http.getBaseData(listener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d(TAG, volleyError.toString());
            }
        }, dataList, AccessToken.getCurrentAccessToken().getToken());

    }

}
