package pl.slapps.motiv.motivrestaurant.orders;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.fragment.SmartPitPagerFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.gryko.smartpitlib.widget.SmartPitTabHostIndicator;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.background.BackgroundService;
import pl.slapps.motiv.motivrestaurant.model.Order;
import pl.slapps.motiv.motivrestaurant.widget.AppHelper;
import pl.slapps.motiv.motivrestaurant.widget.CustomSlidingPane;

/**
 * Created by piotr on 16.03.15.
 */
public class FragmentOrdersBase extends SmartPitFragment {

    private String TAG = FragmentOrdersBase.this.getClass().getName();


    private LinearLayout contentOne;
    private CustomSlidingPane pane;
    private CustomSlidingPane paneTwo;


    private FragmentMap fragmentMap;
    private FragmentOrdersLists fragmentOrdersLists;
    private FragmentOrderDetails fragmentOrderDetails;


    private String label = "Zamowienia";
    private Handler handler = new Handler();
    private Runnable refreashTask = new Runnable() {
        @Override
        public void run() {
            refreashData();
        }
    };

    private BackgroundService backgroundService;


    public CustomSlidingPane getPane() {
        return pane;
    }

    public CustomSlidingPane getPaneTwo() {
        return paneTwo;
    }

    public boolean onBackPressed() {
        if (paneTwo != null && !paneTwo.isOpen()) {
            paneTwo.openPane();
            return true;
        } else if (!pane.isOpen()) {
            pane.openPane();
            return true;
        } else
            return false;
    }


    public void onDestroyView() {
        super.onDestroyView();
        if (backgroundService != null)
            backgroundService.setOnOrdersUpdateListener(null);
    }



    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_orders_base, parent, false);
        ((MainActivity) this.getActivity()).showActionbar();

        backgroundService = ((MainActivity) this.getActivity()).getService();
        if (backgroundService != null) {
            backgroundService.setOnOrdersUpdateListener(new BackgroundService.OnOrdersUpdateListener() {
                @Override
                public void onStatusChanged(JSONObject result) {
                    Log.d(TAG, "status changed " + result);

                    handler.post(refreashTask);
                }

                @Override
                public void onCoordsChanged(JSONObject result) {
                    Log.d(TAG, "choords changed " + result);

                }
            });
        }
        contentOne = (LinearLayout) v.findViewById(R.id.content_one);

        if (this.getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            contentOne.getLayoutParams().width = SmartPitAppHelper.getScreenWidth(this.getActivity()) / 3;
        }

        pane = (CustomSlidingPane) v.findViewById(R.id.pane);
        paneTwo = (CustomSlidingPane) v.findViewById(R.id.pane_two);


        fragmentMap = new FragmentMap();
        fragmentOrdersLists = new FragmentOrdersLists();
        fragmentOrderDetails = new FragmentOrderDetails();
        fragmentMap.setHost(this);

        pane.setPanelSlideListener(new SlidingPaneLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelOpened(View panel) {
                fragmentMap.resetCameraBasedOnMarkers();
                label = "Zamówienia";
                setActionbarLabel();

            }

            @Override
            public void onPanelClosed(View panel) {
            }
        });
        this.getChildFragmentManager().beginTransaction().add(R.id.content_one, fragmentOrdersLists).add(R.id.content_two, fragmentMap).add(R.id.content_three, fragmentOrderDetails).commitAllowingStateLoss();


        new Handler().post(new Runnable() {
            @Override
            public void run() {
                pane.openPane();

                if (paneTwo != null)
                    paneTwo.openPane();

            }
        });


        return v;
    }

    public int getCurrentTab() {
        return fragmentOrdersLists.getPager().getCurrentItem();
    }

    public void initDetailView(final Order order) {
        pane.closePane();
        if (paneTwo != null)
            paneTwo.closePane();
        fragmentOrderDetails.initDetailView(order);
    }

    public void toggleRoute(Order order) {
        fragmentMap.toggleRoute(order);
    }

    public void centerMarker(Order order) {
        fragmentMap.centerMarker(order);
    }

    public void refreashMap() {
        fragmentMap.refreashSelf();
    }


    public void refreashData() {
        fragmentOrdersLists.refreashData();
        fragmentMap.refreashSelf();

    }


    @Override
    public String getLabel() {

        return label;
    }


}
