package pl.slapps.motiv.motivrestaurant.orders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;


import java.util.ArrayList;

import pl.gryko.smartpitlib.interfaces.SmartPitFragmentsInterface;
import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.model.Order;

/**
 * Created by piotr on 16.03.15.
 */
public class FragmentOrdersList {

    private String TAG = FragmentOrdersList.class.getName();

    private ListView listView;
    private Context context;
    private SmartPitFragmentsInterface listener;
    private ArrayList<Order> data;
    private FragmentOrdersBase parent;

    private ProgressBar bar;

    public View init(FragmentOrdersBase parent, final SmartPitFragmentsInterface listener, final ArrayList<Order> data) {
        this.context = parent.getActivity();
        this.parent=parent;
        this.listener = listener;
        this.data = data;
        View v = LayoutInflater.from(context).inflate(R.layout.fragment_list, null);
        listView = (ListView) v.findViewById(R.id.lv);
        bar = (ProgressBar) v.findViewById(R.id.bar);

        if (data.size() > 0)
            bar.setVisibility(View.GONE);

        refreashData(data);

        return v;
    }

    public void refreashData(final ArrayList<Order> data) {

        Log.d(TAG, "refresh data " + data.size());
        try {


            if (data.size() > 0 && bar.getVisibility() == View.VISIBLE)
                bar.setVisibility(View.GONE);
            listView.setAdapter(new AdapterOrdersList(context, data, this.listener));
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    FragmentOrdersList.this.parent.initDetailView(data.get(position));


                }
            });

        } catch (Throwable t) {
            Log.d(TAG, t.toString());
        }
    }


}
