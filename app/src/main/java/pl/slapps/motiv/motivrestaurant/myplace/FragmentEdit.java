package pl.slapps.motiv.motivrestaurant.myplace;

import android.app.TimePickerDialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;


import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartPitAppHelper;
import pl.gryko.smartpitlib.widget.SmartPitRowedLayout;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.adapter.AdapterGridGallery;
import pl.slapps.motiv.motivrestaurant.fragment.FragmentGallery;
import pl.slapps.motiv.motivrestaurant.model.Period;

/**
 * Created by piotr on 13.04.15.
 */
public class FragmentEdit extends SmartPitFragment {

    private String TAG = FragmentEdit.class.getName();

    //private ArrayList<View> viewsList;
    private View view;
    private LayoutInflater inflater;

    //private LinearLayout top_layout;
    //private SmartImageView ivCover;


    public static boolean PICK_LOGO;
    public static boolean PICK_GALLERY;

    private RelativeLayout contentOne;
    private LinearLayout contentTwo;

    private TextView facebookAbout;
    private TextView facebookDesc;
    private TextView facebookBirthday;
    private TextView facebookCategory;
    private TextView facebookWebsite;
    private TextView facebookLkes;
    private TextView facebookWerehere;
    private TextView facebookTalkAbout;

    private SmartPitSelectorView cameraPick;
    private SmartPitSelectorView galleryPick;


    private EditText etName;
    private EditText etPhone;
    private EditText etAddress;
    private EditText etDesc;
    private EditText etWebsite;
    private SmartPitRowedLayout tags;
    private SlidingPaneLayout pane;
    private GridView gridView;
    private JSONArray photos;
    private LinearLayout openingHours;
    private SimpleDateFormat sdfIn = new SimpleDateFormat("HHmm");
    private SimpleDateFormat sdfOut = new SimpleDateFormat("HH:mm");
    private SmartPitSelectorView btnSave;


    private View getTagView(String text) {
        View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.tag_layout, null);
        TextView tv = (TextView) v.findViewById(R.id.tv_label);

        tv.setText(text);
        return v;
    }

    private View getOpeningHourView(int day, String from, String to) {
        View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.row_opening_hours, null);

        TextView tvDay = (TextView) v.findViewById(R.id.tv_day);
        final TextView tvFrom = (TextView) v.findViewById(R.id.tv_from);
        final TextView tvTo = (TextView) v.findViewById(R.id.tv_to);

        switch (day) {
            case 0:
                tvDay.setText("Poniedziałek");
                break;
            case 1:
                tvDay.setText("Wtorek");
                break;
            case 2:
                tvDay.setText("Środa");
                break;
            case 3:
                tvDay.setText("Czwartek");
                break;
            case 4:
                tvDay.setText("Piątek");
                break;
            case 5:
                tvDay.setText("Sobota");
                break;
            case 6:
                tvDay.setText("Niedziela");
                break;
        }

        try {
            final Calendar fromTime = Calendar.getInstance();
            final Calendar toTime = Calendar.getInstance();
            fromTime.setTime(sdfIn.parse(from));
            toTime.setTime(sdfIn.parse(to));
            from = sdfOut.format(fromTime.getTime());
            to = sdfOut.format(toTime.getTime());

            tvFrom.setText(from);
            tvTo.setText(to);

            tvFrom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new TimePickerDialog(FragmentEdit.this.getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int i, int i1) {
                            fromTime.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
                            fromTime.set(Calendar.MINUTE, timePicker.getCurrentMinute());

                            tvFrom.setText(sdfOut.format(fromTime.getTime()));
                        }
                    }, fromTime.get(Calendar.HOUR_OF_DAY), fromTime.get(Calendar.MINUTE), true).show();
                }
            });

            tvTo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new TimePickerDialog(FragmentEdit.this.getActivity(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int i, int i1) {
                            toTime.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
                            toTime.set(Calendar.MINUTE, timePicker.getCurrentMinute());

                            tvTo.setText(sdfOut.format(toTime.getTime()));
                        }
                    }, toTime.get(Calendar.HOUR_OF_DAY), toTime.get(Calendar.MINUTE), true).show();
                }
            });

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return v;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_place_edit, parent, false);
        pane=(SlidingPaneLayout)v.findViewById(R.id.pane);
        contentOne = (RelativeLayout) v.findViewById(R.id.content_one);
        contentTwo = (LinearLayout) v.findViewById(R.id.content_two);

        if (this.getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

            contentOne.getLayoutParams().width = SmartPitAppHelper.getScreenWidth(this.getActivity()) / 2;
            contentTwo.getLayoutParams().width = SmartPitAppHelper.getScreenWidth(this.getActivity()) / 2;
        }

        facebookCategory = (TextView) v.findViewById(R.id.facebook_category);
        facebookAbout = (TextView) v.findViewById(R.id.facebook_about);
        facebookDesc = (TextView) v.findViewById(R.id.facebook_desc);
        facebookLkes = (TextView) v.findViewById(R.id.facebook_likes);
        facebookWerehere = (TextView) v.findViewById(R.id.facebook_were_here);
        facebookWebsite = (TextView) v.findViewById(R.id.facebook_web);
        facebookTalkAbout = (TextView) v.findViewById(R.id.facebook_talk_about);
        facebookBirthday = (TextView) v.findViewById(R.id.facebook_birthday);

        btnSave = (SmartPitSelectorView) v.findViewById(R.id.btn_save);

        btnSave.configure(R.drawable.circle_green_light, R.drawable.circle_green, R.drawable.save);

        int padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);

        btnSave.getImageView().setPadding(padding, padding, padding, padding);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        facebookAbout.setText(MyApplication.currentRestaurant.facebookAbout);
        facebookDesc.setText(MyApplication.currentRestaurant.facebookDesc);
        facebookBirthday.setText(MyApplication.currentRestaurant.facebookBirthday);
        facebookCategory.setText(MyApplication.currentRestaurant.facebookCategory);
        facebookWebsite.setText(MyApplication.currentRestaurant.facebookWebsite);

        facebookLkes.setText(Integer.toString(MyApplication.currentRestaurant.facebookLikes));
        facebookWerehere.setText(Integer.toString(MyApplication.currentRestaurant.facebookWereHere));
        facebookTalkAbout.setText(Integer.toString(MyApplication.currentRestaurant.facebookTalkingAbout));


        gridView = (GridView) v.findViewById(R.id.grid_view);
        //tvName = (TextView) v.findViewById(R.id.tv_name);
        etName = (EditText) v.findViewById(R.id.et_name);

        //  tvPhone = (TextView)v.findViewById(R.id.tv_phone);
        etPhone = (EditText) v.findViewById(R.id.et_phone);
        etWebsite = (EditText) v.findViewById(R.id.et_website);


        //tvAddress = (TextView) v.findViewById(R.id.tv_address);
        etAddress = (EditText) v.findViewById(R.id.et_address);


        //tvDesc = (TextView) v.findViewById(R.id.tv_desc);
        etDesc = (EditText) v.findViewById(R.id.et_desc);

        tags = (SmartPitRowedLayout) v.findViewById(R.id.et_tags);

        openingHours = (LinearLayout) v.findViewById(R.id.base_opening);

        etName.setText(MyApplication.currentRestaurant.name);
        etPhone.setText(MyApplication.currentRestaurant.formatted_phone_number);
        etAddress.setText(MyApplication.currentRestaurant.formatted_address);
        etWebsite.setText(MyApplication.currentRestaurant.website);

        for (int i = 0; i < MyApplication.currentRestaurant.periods.size(); i++) {
            Period p = MyApplication.currentRestaurant.periods.get(i);
            openingHours.addView(getOpeningHourView(p.day, p.open_time, p.close_time));
        }


        for (int i = 0; i < MyApplication.currentRestaurant.tags.length; i++) {
            tags.addView(getTagView(MyApplication.currentRestaurant.tags[i]));
        }

        photos = new JSONArray();
        for (int i = 0; i < MyApplication.currentRestaurant.photos.size(); i++) {
            photos.put(MyApplication.currentRestaurant.photos.get(i));
        }

        gridView.setAdapter(new AdapterGridGallery(this.getActivity(), MyApplication.currentRestaurant.photos));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                ((FragmentGallery) ((MainActivity) FragmentEdit.this.getActivity()).getCurrentDrawerFragment()).refreashPager(photos.toString(), i);
                ((MainActivity) FragmentEdit.this.getActivity()).showMenu();

            }
        });


        cameraPick = (SmartPitSelectorView) v.findViewById(R.id.btn_camera_file);
        galleryPick = (SmartPitSelectorView) v.findViewById(R.id.btn_gallery_file);

        cameraPick.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.camera_white);
        cameraPick.getImageView().setPadding(padding, padding, padding, padding);

        galleryPick.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.white_gallery);
        galleryPick.getImageView().setPadding(padding, padding, padding, padding);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                pane.openPane();
            }
        });

        return v;
    }

    public boolean onBackPressed() {
        if (((MainActivity) FragmentEdit.this.getActivity()).getDrawerLayout().isDrawerOpen(((MainActivity) FragmentEdit.this.getActivity()).getDrawerContent())) {
            ((MainActivity) FragmentEdit.this.getActivity()).showMenu();
            return true;
        }
        return super.onBackPressed();
    }


    @Override
    public String getLabel() {
        return null;
    }


}
