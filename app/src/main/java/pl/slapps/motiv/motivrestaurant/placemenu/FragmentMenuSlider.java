package pl.slapps.motiv.motivrestaurant.placemenu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.Log;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 04.09.15.
 */
public class FragmentMenuSlider extends SmartPitFragment {

    private String TAG = FragmentMenuSlider.class.getName();
    private ListView lv;
    private FragmentMenuTree host;
    private ArrayList<JSONObject> data;
    private AdapterMenuSlider adapter;

    public void setHost(FragmentMenuTree host) {
        this.host = host;
    }


    private void initBlankView() {
        JSONObject element = new JSONObject();
        try {
            data = new ArrayList<>();
            element.put("active", true);
            element.put("label", "Wszystkie");
            data.add(element);
            lv.setAdapter(new AdapterMenuSlider(this.getActivity(), data));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initRootView() {
        setListListener();
        JSONObject parent_cat = null;
        try {
            parent_cat = new JSONObject(this.getArguments().getString("parent"));

            final String parent_id = parent_cat.has("_id") ? parent_cat.getString("_id") : "";
            DAO.getCategories(MyApplication.currentRestaurant.id, new Response.Listener() {
                @Override
                public void onResponse(Object o) {
                    data = new ArrayList<JSONObject>();
                    Log.d(TAG, o.toString());
                    JSONArray array = null;
                    try {
                        JSONObject result = new JSONObject(o.toString());
                        result = result.has("api") ? result.getJSONObject("api") : new JSONObject();

                        array = result.has("getChildren") ? result.getJSONArray("getChildren") : (result.has("results") ? result.getJSONArray("results") : new JSONArray());


                        for (int i = 0; i < array.length(); i++) {

                            JSONObject element = array.getJSONObject(i);


                            data.add(element);

                        }
                        if (lv != null && FragmentMenuSlider.this.isAdded()) {
                            adapter = new AdapterMenuSlider((MainActivity) FragmentMenuSlider.this.getActivity(), data);
                            adapter.setActiveRow(parent_id);
                            lv.setAdapter(adapter);



                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d(TAG, volleyError.toString());
                }
            }, null);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initChildView() {
        setListListener();
        try {

            JSONObject parent_cat = new JSONObject(this.getArguments().getString("parent"));

            final String parent_id = parent_cat.has("_id") ? parent_cat.getString("_id") : "";

            JSONObject owner_cat = new JSONObject(this.getArguments().getString("owner"));

            final String owner_id = owner_cat.has("_id") ? owner_cat.getString("_id") : "";


            DAO.getCategories(MyApplication.currentRestaurant.id, new Response.Listener() {
                @Override
                public void onResponse(Object o) {
                    data = new ArrayList<JSONObject>();

                    Log.d(TAG, o.toString());
                    JSONArray array = null;
                    try {
                        JSONObject result = new JSONObject(o.toString());
                        result = result.has("api") ? result.getJSONObject("api") : new JSONObject();

                        array = result.has("getChildren") ? result.getJSONArray("getChildren") : (result.has("results") ? result.getJSONArray("results") : new JSONArray());


                        for (int i = 0; i < array.length(); i++) {
                            JSONObject element = array.getJSONObject(i);
                            //  String element_id = element.has("_id") ? element.getString("_id") : "";
                            //  if (element_id.equals(parent_id)) {
                            //      element.put("active", true);
                            //  }
                            data.add(element);

                        }
                        //if (FragmentRestaurantChildCategories.this.isAdded())
                        adapter = new AdapterMenuSlider((MainActivity) FragmentMenuSlider.this.getActivity(), data);
                        adapter.setActiveRow(parent_id);
                        lv.setAdapter(adapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.d(TAG, volleyError.toString());
                }
            }, owner_id);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void setListListener() {
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (host != null) {
                    Bundle arg = new Bundle();
                    arg.putString("parent", data.get(i).toString());
                    adapter.setActiveRow(adapter.getItemID(i));
                    adapter.notifyDataSetChanged();
                    host.refreashSecondPane(arg);
                }
            }

        });
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menu_slider, parent, false);

        lv = (ListView) v.findViewById(R.id.lv);


        if (this.getArguments() == null) {
            initBlankView();
        } else if (this.getArguments().getString("owner") == null) {
            initRootView();

        } else {
            initChildView();
        }


        return v;
    }

    @Override
    public String getLabel() {
        return null;
    }
}
