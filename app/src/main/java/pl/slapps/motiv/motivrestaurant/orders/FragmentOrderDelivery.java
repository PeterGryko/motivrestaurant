package pl.slapps.motiv.motivrestaurant.orders;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.adapter.SmartPitGoogleAddressesAdapter;
import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.MyApplication;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.fragment.FragmentMainMenu;

/**
 * Created by piotr on 05.09.15.
 */
public class FragmentOrderDelivery extends SmartPitFragment {

    private String TAG = FragmentOrderDelivery.class.getName();
    private LinearLayout layoutMap;
    private LinearLayout layoutContent;
    private ListView lv;
    private AdapterSummaryCart adapter;
    private ArrayList<JSONObject> dataList;

    private EditText etClient;
    private EditText etPhone;
    private AutoCompleteTextView etStreet;
    private EditText etHouse;
    private EditText etFlat;
    private EditText etNote;
    private SmartPitSelectorView buy;
    private TextView tvPriceSum;
    private SmartPitGoogleAddressesAdapter adapterStreet;

    private double lat;
    private double lon;

    @Override
    public String getLabel() {
        return null;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_deliver, parent, false);



        lv = (ListView) v.findViewById(R.id.lv);
        buy = (SmartPitSelectorView) v.findViewById(R.id.bottom);
        etClient = (EditText) v.findViewById(R.id.et_client);
        etPhone = (EditText) v.findViewById(R.id.et_phone);
        etStreet = (AutoCompleteTextView) v.findViewById(R.id.et_street);
        etFlat = (EditText) v.findViewById(R.id.et_flat);
        etHouse = (EditText) v.findViewById(R.id.et_house);
        etNote = (EditText) v.findViewById(R.id.et_note);

        tvPriceSum = (TextView)v.findViewById(R.id.tv_price_sum);

        int padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);
        buy.configure(R.drawable.circle_green_light, R.drawable.circle_red, R.drawable.checkin);
        buy.getImageView().setPadding(padding, padding, padding, padding);


        adapterStreet = new SmartPitGoogleAddressesAdapter(this.getActivity(), android.R.layout.simple_dropdown_item_1line);
        etStreet.setAdapter(adapterStreet);


        etStreet.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                lat = adapterStreet.getPoint(i).getLat();
                lon = adapterStreet.getPoint(i).getLon();

            }
        });


        dataList = new ArrayList<>();
        if (this.getArguments() != null) {
            try {
                final JSONArray array = new JSONArray(this.getArguments().getString("data"));
                final String price = this.getArguments().getString("price");
                tvPriceSum.setText(price);
                for (int i = 0; i < array.length(); i++) {
                    dataList.add(array.getJSONObject(i));
                }
                adapter = new AdapterSummaryCart(this.getActivity(), dataList);
                lv.setAdapter(adapter);


                buy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        JSONObject data = new JSONObject();
                        try {

                            String client = etClient.getText().toString();
                            String phone = etPhone.getText().toString();
                            String street = etStreet.getText().toString();
                            String flat = etFlat.getText().toString();
                            String house = etHouse.getText().toString();
                            String note = etNote.getText().toString();

                            if (lat == 0 || lon == 0) {
                                Toast.makeText(FragmentOrderDelivery.this.getActivity(), "Musisz wybrać adres", Toast.LENGTH_LONG).show();
                                return;
                            } else if (client.trim().equals("")
                                    || phone.trim().equals("")
                                    || street.trim().equals("")
                                    || flat.trim().equals("")
                                    || house.trim().equals("")) {
                                Toast.makeText(FragmentOrderDelivery.this.getActivity(), "Musisz wypełnić wszystkie pola", Toast.LENGTH_LONG).show();
                                return;
                            }

                            data.put("cart", array);
                            data.put("owner", MyApplication.currentRestaurant.id);
                            JSONObject details = new JSONObject();
                            details.put("on_place", false);
                            details.put("client", client);
                            details.put("phone", phone);
                            details.put("street", street);
                            details.put("flat", flat);
                            details.put("house", house);
                            details.put("note", note);
                            details.put("lat", lat);
                            details.put("lon", lon);
                            details.put("price", price);


                            data.put("deliveryData", details);

                            DAO.createOrder(data, new Response.Listener() {
                                @Override
                                public void onResponse(Object o) {

                                    Log.d(TAG, o.toString());
                                    ((MainActivity)FragmentOrderDelivery.this.getActivity()).switchTitleFragment(new FragmentMainMenu(), true);
                                    Toast.makeText(FragmentOrderDelivery.this.getActivity(),"Zamówienie zostalo złożone",Toast.LENGTH_LONG).show();
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {
                                    Log.d(TAG, volleyError.toString());
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //layoutContent=(LinearLayout)v.findViewById(R.id.layout_content);
        //layoutMap= (LinearLayout)v.findViewById(R.id.layout_map);

        //this.getChildFragmentManager().beginTransaction().add(R.id.layout_map,new FragmentMap()).commitAllowingStateLoss();

        return v;
    }
}
