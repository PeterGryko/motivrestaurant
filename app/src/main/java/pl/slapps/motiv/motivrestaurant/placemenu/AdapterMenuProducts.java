package pl.slapps.motiv.motivrestaurant.placemenu;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.gryko.smartpitlib.bitmaps.SmartPitImageLoader;
import pl.gryko.smartpitlib.widget.SmartImageView;
import pl.slapps.motiv.motivrestaurant.DAO.DAO;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 03.09.15.
 */
public class AdapterMenuProducts extends ArrayAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<JSONObject> data;

    private String active;

    public AdapterMenuProducts(Context context, ArrayList<JSONObject> data) {
        super(context, R.layout.row_menu_product, data);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getItemID(int position) {
        JSONObject element = data.get(position);
        String id = null;
        try {
            id = element.has("_id") ? element.getString("_id") : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return id;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        View v = inflater.inflate(R.layout.row_menu_product, null);


        TextView label = (TextView) v.findViewById(R.id.tv_label);
        TextView tvPrice = (TextView) v.findViewById(R.id.tv_price);

        SmartImageView imageview = (SmartImageView) v.findViewById(R.id.image);
        imageview.setMode(SmartImageView.Mode.NORMAL.ordinal());
        imageview.getImageView().setScaleType(ImageView.ScaleType.CENTER_CROP);

        JSONObject element = data.get(position);
        ListTriangle triangle = (ListTriangle) v.findViewById(R.id.triangle);
        triangle.paint.setColor(Color.parseColor("#eeeeee"));

        triangle.setVisibility(View.GONE);

        try {
            String name = element.has("name") ? element.getString("name") : "";

            String id = element.has("_id") ? element.getString("_id") : "";
            label.setText("#" + position + " " + name);

            JSONObject data = element.has("data") ? element.getJSONObject("data") : new JSONObject();
            String price = data.has("price") ? data.getString("price") : "";
            tvPrice.setText(price + "zł");


            JSONArray photos = element.has("photos") ? element.getJSONArray("photos") : new JSONArray();

            if (photos.length() > 0) {
                JSONObject photo = photos.getJSONObject(0);
                String path = photo.has("path") ? photo.getString("path") : "";

                if (!path.trim().equals(""))
                    SmartPitImageLoader.setImage(context, imageview, DAO.API_STATIC + path, 0, 0);

            } else {
                imageview.getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.mock_product));
                imageview.getProgressBar().setVisibility(View.GONE);
                imageview.getImageView().setScaleType(ImageView.ScaleType.CENTER_INSIDE);

            }

            if (active != null && active.equals(id))
                triangle.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return v;
    }
}
