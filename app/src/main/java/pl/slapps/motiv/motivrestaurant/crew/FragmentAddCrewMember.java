package pl.slapps.motiv.motivrestaurant.crew;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.gryko.smartpitlib.widget.SmartPitRowedLayout;
import pl.gryko.smartpitlib.widget.SmartPitSelectorView;
import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.model.User;

/**
 * Created by piotr on 27.09.15.
 */
public class FragmentAddCrewMember extends SmartPitFragment {

    private SmartPitRowedLayout rolesBase;

    private SmartPitSelectorView btnSave;

    private EditText etName;
    private EditText etEmail;
    private EditText etPhone;
    private EditText etCity;
    private EditText etAddress;
    private EditText etFlat;
    private EditText etHouse;
    private EditText etPostCode;

    private View getRoleLabelView(String data) {
        View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.role_layout, null);
        TextView label = (TextView) v.findViewById(R.id.tv_label);
        label.setText(data);
        return v;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crew_new_member, parent, false);

        etName = (EditText)v.findViewById(R.id.et_name);
        etEmail = (EditText)v.findViewById(R.id.et_email);
        etPhone = (EditText)v.findViewById(R.id.et_phone);
        etPostCode = (EditText)v.findViewById(R.id.et_code);
        etHouse = (EditText)v.findViewById(R.id.et_house);
        etFlat = (EditText)v.findViewById(R.id.et_flat);
        etAddress = (EditText)v.findViewById(R.id.et_address);
        etCity = (EditText)v.findViewById(R.id.et_city);


        rolesBase = (SmartPitRowedLayout) v.findViewById(R.id.base_roles);



        btnSave = (SmartPitSelectorView) v.findViewById(R.id.btn_save);
        btnSave.configure(R.drawable.circle_green_light, R.drawable.circle_green, R.drawable.save);
        int padding = (int) this.getActivity().getResources().getDimension(R.dimen.btn_big_padding);
        btnSave.getImageView().setPadding(padding, padding, padding, padding);

        if (this.getArguments() != null) {
            String arg = this.getArguments().getString("data");
            try {
                JSONObject data = new JSONObject(arg);
                User u = new User(data);
                etCity.setText(u.locality);
                etPhone.setText(u.phone);
                etName.setText(u.name);
                etPostCode.setText(u.postCode);
                etAddress.setText(u.address);
                rolesBase.addView(getRoleLabelView(u.role));


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        return v;
    }

    @Override
    public String getLabel() {
        return null;
    }
}
