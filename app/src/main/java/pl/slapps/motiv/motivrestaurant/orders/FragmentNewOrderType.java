package pl.slapps.motiv.motivrestaurant.orders;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import pl.gryko.smartpitlib.fragment.SmartPitFragment;
import pl.slapps.motiv.motivrestaurant.MainActivity;
import pl.slapps.motiv.motivrestaurant.R;

/**
 * Created by piotr on 31.08.15.
 */
public class FragmentNewOrderType extends SmartPitFragment{

    private LinearLayout tabOnPlace;
    private LinearLayout tabDelivery;
    @Override
    public String getLabel() {
        return null;
    }
    public View onCreateView(LayoutInflater inflater,ViewGroup parent, Bundle savedInstanceState)
    {
        View v=  inflater.inflate(R.layout.fragment_new_order_type,parent,false);

        tabDelivery=(LinearLayout)v.findViewById(R.id.tab_delivery);
        tabOnPlace = (LinearLayout)v.findViewById(R.id.tab_on_place);

        tabDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentOrderDelivery fo = new FragmentOrderDelivery();
                fo.setArguments(FragmentNewOrderType.this.getArguments());

                ((MainActivity) FragmentNewOrderType.this.getActivity()).switchFragment(fo, true);            }
        });
        tabOnPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentOrderOnPlace fo = new FragmentOrderOnPlace();
                fo.setArguments(FragmentNewOrderType.this.getArguments());

                        ((MainActivity) FragmentNewOrderType.this.getActivity()).switchFragment(fo, true);
            }
        });
        return v;
    }
}
