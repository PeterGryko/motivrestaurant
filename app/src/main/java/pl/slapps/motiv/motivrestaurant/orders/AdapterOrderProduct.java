package pl.slapps.motiv.motivrestaurant.orders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.slapps.motiv.motivrestaurant.R;
import pl.slapps.motiv.motivrestaurant.placemenu.ListTriangle;

/**
 * Created by piotr on 05.09.15.
 */
public class AdapterOrderProduct extends ArrayAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<JSONObject> data;


    public AdapterOrderProduct(Context context, ArrayList<JSONObject> data) {
        super(context, R.layout.row_order_product, data);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.data = data;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        View v = inflater.inflate(R.layout.row_order_product, null);
        TextView tvLabel = (TextView) v.findViewById(R.id.tv_name);
        TextView tvCount = (TextView) v.findViewById(R.id.tv_count);


        JSONObject element = data.get(position);


        try {
            int count = element.has("count")?element.getInt("count"):0;
            JSONObject product = element.has("product")?element.getJSONObject("product"):new JSONObject();
            String name = product.has("name") ? product.getString("name") : "";

            tvLabel.setText(name);
            tvCount.setText(count+"x ");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return v;
    }
}
